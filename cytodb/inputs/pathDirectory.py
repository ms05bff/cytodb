import os

# File to manage path

# Entry of the script
ROOT_DIR = os.path.dirname(__file__)
pos_root = ROOT_DIR.find("cytodb")
ROOT_DIR = ROOT_DIR[:pos_root]

input_directory = ROOT_DIR + "cytodb/"
# Entry where data is stored
# Temporary folder that will move
# data_entry = "D:/"
# FOR WINDOWS
# data_entry = "//datawork/datawork-pelagos-s/intranet/kevin_cyto/"
# FOR LINUX
data_entry = "D:/Kevin/Travail/BDD/"
base_directory = data_entry + "base/"
data_to_add = base_directory + "A TRAITER/"
home_directory = data_to_add + "CSV/"
test_directory = base_directory + 'CSV/cytodb_test/'

export_directory = base_directory

# Information directory where we find import_***.csv
# Temporary folder that will move
infos_directory = data_to_add + "IMPORT/"

# Name of the output
output_file = "cytodb.csv"
