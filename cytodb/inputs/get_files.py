import sys
import dateutil.parser as dparser
import numpy as np

# from pathDirectory import *

from cytodb.inputs.pathDirectory import *

def test_mode(path_CYZ, path_CSV):
    """
    Check data organisation. Return False if CYZ find
    :param path_mission: path CYZ to mission
    :param path_CSV: path CSV to mission
    :return: bool, list_files
    """
    Cyz_disable = True
    # If got CYZ, enable this option
    try:
        for root, directory, files in os.walk(path_CYZ):
            if not files:
                print('No CYZ files for this mission')
            else:
                Cyz_disable = False
    except:
        pass
    means_path = []
    # Else, get cyz name from means_per_set
    for root, directory, files in os.walk(path_CSV):
        if not files:
            # If no file in folder
            print('No means files find')
        else:
            # Browse folder to find all files means_per_set
            for dir in directory:
                for subroot, subdirectory, subfiles in os.walk(path_CSV + '/' + dir):
                    for file in subfiles:
                        if file.find('means_per_set') >= 0:
                            means_path.append(subroot + '/' + file)
            for file in files:
                if file.find('means_per_set') >= 0:
                    means_path = root + '/' + file
                    break
        break
    return Cyz_disable, means_path

def main():
    """
    This script is used to get name files & add them in
    the file import called "model_import.csv"
    """
    # Get name of mission
    # sys.argv[1] = get first argument after command
    # python3 get_files.py suivi_culture
    mission = 'suivi_culture'  # sys.argv[1]
    path_CYZ = data_to_add + "CYZ/" + mission + '/'
    path_CSV = data_to_add + "CSV/" + mission + '/'

    path_ = data_entry + "base/IMPORT/"
    path_full = path_ + "modele_import.csv"
    array_model = np.array(np.loadtxt(path_full, delimiter=";", dtype=str))
    path_save = data_to_add + 'IMPORT/' + "import_" + mission + ".csv"

    test, means_path = test_mode(path_CYZ, path_CSV)
    if not test:
        data = mode_cyz(path_CYZ)
    else:
        data = mode_csv(means_path)
    fill_with_data(data, array_model, path_save)

def mode_cyz(path_CYZ):
    """
    If CYZ; browse micro/pico folder
    :param path_mission:
    :return: list of str, list of str, list of str, list of str
    """
    path_micro = path_CYZ + "micro/"
    path_pico = path_CYZ + "pico/"
    list_file = []
    # Empty array, to be filled
    zeros_array = np.zeros((len(list_file), 11), dtype='U50')
    # Get micro files
    for root, directory, files in os.walk(path_micro):
        for file in files:
            if file[-3:] == 'cyz':
                list_file.append(file)
    #Get pico files
    for root, directory, files in os.walk(path_pico):
        for file in files:
            if file[-3:] == 'cyz':
                list_file.append(file)
    # Extract date from file name
    sampling_date, processing_date, processing_hour = [], [], []
    for file in list_file:
        # If it's not backup file
        if file.find("db") < 0:
            # Split file name without extension
            file_split = file[:-4].split()
            try:
                date = dparser.parse(file_split[0], dayfirst=True)
                sampling_date.append(str(date))
            except:
                sampling_date.append('')
            processing_date.append(file_split[1])
            processing_hour.append(file_split[2])
    # If mistake occured, operator might manually change it
    return list_file, sampling_date, processing_date, processing_hour

def mode_csv(means_path):
    """
    Extract data from means_file
    :param means_path: path to file, str
    :return: list of str, list of str, list of str, list of str
    """
    array = np.array(np.loadtxt(means_path, delimiter=";", dtype=str, skiprows=1, usecols=0))
    arg_to_remove = []
    first_file = None
    for i in range(len(array[:])):
        if array[i] == first_file:
            arg_to_remove.append(i)
        else:
            first_file = array[i]
    array_cyz = np.delete(array, arg_to_remove)
    processing_date, processing_hour = [], []
    for row in array_cyz:
        pos_space = row.find(' ')
        try:
            date = dparser.parse(row[pos_space:], fuzzy=True)
            date_split = str(date).split(' ')
            processing_date.append(date_split[0])
            processing_hour.append(date_split[1])
        except:
            pass
    sampling_date = ['']* len(array_cyz)
    return array_cyz, sampling_date, processing_date, processing_hour


def fill_with_data(data, array_model, path_save):
    """
    :param data:
    :param array_model:
    :param path_save:
    """
    # List of column to fill
    list_item = ['File to process', 'Sampling Date', 'Processing Date', 'Processing Hour']
    row_pos = int(np.where(array_model[:, 0] == 'Name of mission')[0])
    zeros_array = np.zeros((len(data[0]), 11), dtype='U50')
    for i in range(len(list_item)):
        column_pos = int(np.where(array_model[row_pos, :] == list_item[i])[0])
        zeros_array[:, column_pos] = np.array(data[i])
    array = np.append(array_model, zeros_array, axis=0)
    np.savetxt(path_save, array, delimiter=";", fmt="%s")
    print('File saved : ' + path_save)


if __name__ == "__main__":
    # Launch after reading cytodb.doc
    main()
    print("End of the script")
