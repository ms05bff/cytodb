# We define each table
# -----------------------------------------------------------------------------
values_csv = [
    "Project",
    "Metadata.Flag",
    "Cluster.Flag",
    "Samples.Flag",
    "Cytometer.Flag",
    "Global.Quality",
    "Trigg_lvl",
    "Station",
    "Cytometer.ID",
    "File",
    "Mean.Total.FWS_varx1",
    "SD.Total.FWS_varx2",
    "Mean.Total.SWS",
    "SD.Total.SWS",
    "Mean.Total.FLR",
    "SD.Total.FLR",
    "Mean.Total.FLO",
    "SD.Total.FLO",
    "Mean.Total.FLG",
    "SD.Total.FLG",
    "Mean.Total.FLY",
    "SD.Total.FLY",
    "Mean.Length",
    "SD.Length",
    "Total_Count",
    "Volume"
]
values_table = """CREATE TABLE IF NOT EXISTS values_echant (
                                    id_values_echant integer primary key,
                                    mission text,
                                    metadata_flag integer,
                                    cluster_flag integer,
                                    samples_flag integer,
                                    cytometer_flag integer,
                                    global_quality integer, 
                                    [trigger_level(mV)] text,
                                    station text,
                                    equipment text,
                                    filename text, 
                                    mean_total_fws real, 
                                    sd_total_fws real, 
                                    mean_total_sws real, 
                                    sd_total_sws real, 
                                    mean_total_flr real, 
                                    sd_total_flr real, 
                                    mean_total_flo real, 
                                    sd_total_flo real, 
                                    mean_total_flg real, 
                                    sd_total_flg real, 
                                    mean_total_fly real, 
                                    sd_total_fly real, 
                                    mean_length real,
                                    sd_length real, 
                                    total_count integer ,
                                    [volume_echan(uL)] real
                                    ); """
values_insert = """ INSERT INTO values_echant (
                                    mission, 
                                    metadata_flag,
                                    cluster_flag,
                                    samples_flag,
                                    cytometer_flag,
                                    global_quality,
                                    [trigger_level(mV)],
                                    station,
                                    equipment,
                                    filename, 
                                    mean_total_fws, 
                                    sd_total_fws, 
                                    mean_total_sws, 
                                    sd_total_sws, 
                                    mean_total_flr, 
                                    sd_total_flr, 
                                    mean_total_flo, 
                                    sd_total_flo, 
                                    mean_total_flg, 
                                    sd_total_flg, 
                                    mean_total_fly, 
                                    sd_total_fly, 
                                    mean_length,
                                    sd_length,
                                    total_count,
                                    [volume_echan(uL)]
                                    )
              VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) """
# -----------------------------------------------------------------------------
register_csv = [
    "File",
    "Reference_csv",
    "Reference_cyz",
    "Picture_path",
    "All_path",
    "Selection.Set",
    "Standardized.name",
    "Analysis.Date",
    "Sampling.Date",
    "Abundance",
]

register_table = """CREATE TABLE IF NOT EXISTS registers (
                                    id_register integer primary key,
                                    file_name text,
                                    reference_csv text,
                                    reference_cyz text,
                                    path_picture text,
                                    path_all_file text,
                                    selection_set text,
                                    groupe_name text, 
                                    [analysis_date(UTC)] datetime,
                                    [sampling_date(UTC)] datetime,
                                    abundance real
                                    ); """

register_insert = """ INSERT INTO registers (
                                    file_name,
                                    reference_csv,
                                    reference_cyz,
                                    path_picture,
                                    path_all_file,
                                    selection_set,
                                    groupe_name, 
                                    [analysis_date(UTC)],
                                    [sampling_date(UTC)], 
                                    abundance
                                    )
              VALUES(?,?,?,?,?,?,?,?,?,?) """
# -----------------------------------------------------------------------------
settings_csv = [
    "SWS.amplification",
    "FLO.amplification",
    "FLR.amplification",
    "FLY.amplification",
    "FLG.amplification",
    "Trigger.Channel",
    "Trigger.level",
    "Flow_rate"
]
settings_table = """CREATE TABLE IF NOT EXISTS settings (
                                id_information integer primary key,
                                sws_ampli real,
                                flo_ampli real,
                                flr_ampli real,
                                fly_ampli real,
                                flg_ampli real,
                                trigger_channel text, 
                                [trigger_level(mV)] real,
                                [flow_rate(uL/s)] real
                                );"""

settings_insert = """ INSERT INTO settings(                            
                            sws_ampli,
                            flo_ampli,
                            flr_ampli,
                            fly_ampli,
                            flg_ampli,
                            trigger_channel,
                            [trigger_level(mV)],
                            [flow_rate(uL/s)]
                            )
              VALUES(?,?,?,?,?,?,?,?) """
# -----------------------------------------------------------------------------
missions_csv = [
    "Project",
    "Project.starting.Date",
    "Project.ending.Date",
    "Standards.Reference",
    "PI",
    "Samples.Operator",
    "Platform.ID",
    "Platform.Type",
    "Platform.Nationality",
]
missions_table = """CREATE TABLE IF NOT EXISTS missions (
                                id_mission integer primary key,
                                project_name text,
                                project_start text,
                                project_end text,
                                standard_reference text,
                                pi text,
                                samples_operator text,
                                platform_id integer,
                                platform_type integer,
                                platform_nat text
                                );"""
missions_insert = """ INSERT INTO missions(
                                project_name,
                                project_start,
                                project_end,
                                standard_reference,
                                pi,
                                samples_operator,
                                platform_id,
                                platform_type,
                                platform_nat)
          VALUES(?,?,?,?,?,?,?,?,?) """
# -----------------------------------------------------------------------------
station_csv = [
    "Study.area",
    "Latitude",
    "Longitude",
    "Station",
    "Depth",
    "Observation.Type",
    "Clustering.Method",
    "Ship.Code",
    "Ship.Name"
]
station_table = """CREATE TABLE IF NOT EXISTS stations (
                                    id_station integer primary key,
                                    area text,
                                    [latitude(E)] real,
                                    [longitude(N)] real,
                                    nb_station text,
                                    [depth(m)] real,
                                    observ_type text,
                                    clustering_method text,
                                    ship_code text,
                                    ship_name text
                                    );"""
station_insert = """ INSERT INTO stations(
                                area,
                                [latitude(E)],
                                [longitude(N)],
                                nb_station,
                                [depth(m)],
                                observ_type,
                                clustering_method,
                                ship_code,
                                ship_name
                                )
            VALUES(?,?,?,?,?,?,?,?,?) """
# -----------------------------------------------------------------------------
equipment_csv = [
    "Cytometer.ID"
]
equipment_table = """CREATE TABLE IF NOT EXISTS equipments (
                                    id_equipment integer primary key,
                                    id_cytometer integer
                                    );"""

equipment_insert = """ INSERT INTO equipments(
                    id_cytometer)
          VALUES(?) """
# -----------------------------------------------------------------------------

