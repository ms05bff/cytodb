# -----------------------------------------------------------------------------
picture_header = [
    "Particle ID",
    "Filename.csv",
    "Filename",
    "Filename.picture",
    "Groupe.Name",
]

picture_table = """CREATE TABLE IF NOT EXISTS pictures (
                                    id_table integer primary key,
                                    id_picture integer,
                                    filename_csv text,
                                    filename text,
                                    filename_picture text,
                                    groupe_name text
                                    ); """

picture_insert = """ INSERT INTO pictures (
                                    id_picture,
                                    filename_csv,
                                    filename,
                                    filename_picture,
                                    groupe_name
                                    )
              VALUES(?,?,?,?,?) """
# -----------------------------------------------------------------------------
pulse_header = [
    "Particle ID",
    "FWS",
    "SWS",
    "FL.Red",
    "FL.Green",
    "FL.Orange",
    "FL.Yellow",
    "Curvature"
]

pulse_table = """CREATE TABLE IF NOT EXISTS pulses (
                                    id_pulse integer primary key,
                                    id_cells integer, 
                                    fws real,
                                    sws real,
                                    fl_red real,
                                    fl_green real, 
                                    fl_yellow real,
                                    fl_orange real,
                                    curvature real
                                    ); """

pulse_insert = """ INSERT INTO pulses (
                                    id_cells, 
                                    fws,
                                    sws,
                                    fl_red,
                                    fl_green, 
                                    fl_yellow,
                                    fl_orange,
                                    curvature                                  
                                    )
              VALUES(?,?,?,?,?,?,?,?) """
# -----------------------------------------------------------------------------