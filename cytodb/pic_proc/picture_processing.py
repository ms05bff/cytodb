import shutil
import sys
import matplotlib.pyplot as plt
import numpy as np
from skimage import filters, measure, util
from skimage.io import imread
import pandas as pd
from skimage.morphology import closing, square, disk
from skimage.segmentation import clear_border
from cytodb.createConnexion import CreateConnexion
from cytodb.inputs.pathDirectory import *


def get_region(basic_image, label_image):
    area = 0
    if measure.regionprops(label_image):
        for region in measure.regionprops(label_image):
            # take regions with large enough areas
            # to avoid stain
            if region.area >= 100 and region.area > area:
                area = region.area
                # We get position of the stain
                min_y, min_x, max_y, max_x = region.bbox
                pixel_gap = 40
                if min([min_y, min_x]) < pixel_gap:
                    pixel_gap = min([min_y, min_x])
                image_cropped = basic_image[min_y - pixel_gap:max_y + pixel_gap,
                                min_x - pixel_gap:max_x + pixel_gap]
                return image_cropped, region
        if area == 0:
            return None, []
    else:
        return None, []


def process(file, path, background_pic):
    # Check if picture isn't to blurry
    image_ = np.array(imread(path + file, as_gray=True))
    # Reduce noise
    tmp_image = util.img_as_ubyte(filters.gaussian(image_))

    grad = filters.rank.gradient(tmp_image, disk(5))
    max_value_grad = np.max(grad)
    if max_value_grad < 80:
        return None, []
    else:
        # Remove background
        if background_pic:
            background_ = imread(path + background_pic, as_gray=True)
            background_pic = util.img_as_ubyte(filters.gaussian(background_))
            if np.mean(tmp_image)-np.mean(background_pic) > 0:
                tmp_image = tmp_image - background_pic
            else:
                tmp_image = background_pic-tmp_image
            tmp_image = np.where(tmp_image > 255-filters.threshold_otsu(tmp_image), 0, tmp_image)
            thresh = filters.threshold_yen(tmp_image)
            bw = closing(tmp_image >= thresh)
            buf_siz = int(bw.shape[1]/15)
            cleared = clear_border(bw, buffer_size=buf_siz)
            label_image = measure.label(cleared, background=False)
        else:
            # Binarisation : apply threshold
            thresh = filters.threshold_yen(tmp_image)
            bw = closing(tmp_image <= thresh)
            buf_siz = int(bw.shape[1]/15)
            cleared = clear_border(bw, buffer_size=buf_siz)
            # Cleaning : remove artifacts connected to image border
            # label image regions
            label_image = measure.label(cleared, background=False)

        # Segmentation
        image_cropped, info = get_region(image_, label_image)
        return image_cropped, info


def save_picture(picture, file, path_to_save):
    try:
        os.mkdir(path_to_save + '/Cropped')
    except:
        pass
    filename = file.replace('Uncropped', 'Cropped')
    plt.imsave(path_to_save + '/Cropped/' + filename, picture, cmap='gray')


def remove_picture(file):
    os.remove(file)


def keep_old_picture(root, file):
    try:
        os.mkdir(root + '/Uncropped')
    except:
        pass
    shutil.move(root + '/' + file, root + '/Uncropped/' + file)


# def find_informations(path, file, sub_dir):
#     connexion = CreateConnexion(path + 'pythonsqlite.db')
#     pos_ = file[::-1].find(' ')
#     filename = file[:-pos_ + 5]
#     with connexion.conn:
#         cur = connexion.conn.cursor()
#         request = "SELECT DISTINCT  file_name, groupe_name, [sampling_date(UTC)], " \
#                   "[longitude(N)], [latitude(E)], [depth(m)] " + \
#                   "FROM  registers, stations, values_echant " + \
#                   "WHERE registers.file_name =" + "'" + filename + "'" + \
#                   "AND values_echant.filename = " + "'" + filename + "'" + \
#                   "AND stations.nb_station= values_echant.station"
#         cur.execute(request)
#         data = np.array(cur.fetchall())
#     for i in range(len(data)):
#         if sub_dir.find(data[i, 1]) >= 0:
#             return data[i].tolist()
#         else:
#             pass
#     print("Data not found")
#     return []


def find_pulse_file(root, sub_root, file):
    pos_id_cells = file[::-1].find('_')
    id_cells = file[-pos_id_cells:-4]
    pos_folder = sub_root[::-1].find('/')
    folder = sub_root[-pos_folder:]
    filename = root[len(sub_root)+1:].replace('Images', 'Pulses.csv')
    pulse_array = None
    for r, d, f in os.walk(home_directory + folder):
        if f:
            pulse_array = browse_file(r, f, id_cells, filename)
        else:
            for dir in d:
                if filename.find(dir) >= 0:
                    for root, folder, files in os.walk(r + '/' + dir):
                        if files:
                            pulse_array = browse_file(root, files, id_cells, filename)
                            break
        break
    if pulse_array is not None:
        pulse_array[:, 0] = pulse_array[:, 0].astype(int)
        df = pd.DataFrame(pulse_array[:, 1:])
        pulse_array[:, 1:] = np.array(df.apply(lambda x: x.str.replace(',', '.')), dtype=float)
    return pulse_array


def browse_file(r, list_file, id_cells, filename):
    pulse_array = None
    for file in list_file:
        if file.find(filename) >= 0:
            first_column = np.array(np.loadtxt(r + '/' + file, delimiter=";", dtype=str, usecols=0))
            pos_in_column = np.where(first_column == id_cells)[0]
            if not pos_in_column.size == 0:
                header = np.array(np.loadtxt(r + '/' + file, delimiter=";", dtype=str, max_rows=1))

                if len(pos_in_column) > 20:
                    pulse_array = np.array(np.loadtxt(r + '/' + file, delimiter=";", dtype=object,
                                                      skiprows=pos_in_column[0], max_rows=20))
                else:
                    # TODO : test sampling
                    pulse_array = np.array(np.loadtxt(r + '/' + file, delimiter=";", dtype=object,
                                                      skiprows=pos_in_column[0], max_rows=len(pos_in_column)))
                    array_to_add = [[] * len(header)]
                    for i in range(len(header)):
                        array_to_add[i] = np.random.choice(pulse_array[:, i], size=20 - len(pos_in_column))
                    pulse_array = np.vstack((pulse_array, array_to_add))
            else:
                print('Id cells : ' + str(id_cells) + ' not find in ' + file)
            break
    return pulse_array

