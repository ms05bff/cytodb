import numpy as np
import shutil
from scipy import stats
from skimage.io import imread
from cytodb.inputs.pathDirectory import *
from cytodb.createConnexion import CreateConnexion

# Prepare array to save as TSV
# Get data from PICTURE FOLDER / DATABASE


def get_result_request(request):
    with connexion.conn:
        cur = connexion.conn.cursor()
        # test stations
        cur.execute(request)
        result = cur.fetchall()
    return result


path = export_directory + "pythonsqlite.db"
connexion = CreateConnexion(path)
filter_directory = "'Miniscope2017'"
request = "SELECT DISTINCT path_picture FROM registers INNER JOIN values_echant ON registers.file_name = " \
          "values_echant.filename WHERE values_echant.mission = " + filter_directory
result = get_result_request(request)

def get_mode(image):
    flatten_image = image.flatten()
    flatten_image = np.delete(flatten_image, np.where(flatten_image == 255))
    mode_object = stats.mode(flatten_image)[0]
    return mode_object


def get_stats(file):
    image_ = np.array(imread(file, as_gray=True))
    # List header should have same name has header in tsv file
    list_header = [
        "object_MinGrey",
        "object_MaxGrey",
        "object_MeanGreyImage",
        "object_SumGrey",
        "object_ModeGreyObjet",
        "object_SigmaGrey"
    ]
    list_stats = [
        np.min(image_),
        np.max(image_),
        np.mean(image_),
        np.sum(image_),
        get_mode(image_)[0],
        np.std(image_)
    ]

    return list_header, list_stats


def get_metadata(filename):
    request_pictures = "SELECT filename_picture, groupe_name FROM pictures WHERE " \
                       "filename = " + "'" + filename + "'"
    result_picture = get_result_request(request_pictures)
    request_stations = "SELECT DISTINCT stations.[latitude(E)], stations.[longitude(N)], " \
                       "stations.[depth(m)] FROM stations INNER JOIN values_echant ON " \
                       "values_echant.station = stations.nb_station WHERE " \
                       "values_echant.filename = " + "'" + result_picture[0][0] + "'"
    stations = get_result_request(request_stations)
    request_registers = "SELECT [sampling_date(UTC)] FROM registers WHERE " \
                        "registers.file_name = " + "'" + result_picture[0][0] + "' " \
                        "AND groupe_name = " + "'" + result_picture[0][1] + "'"
    registers = get_result_request(request_registers)
    list_header = ["object_lat",
                   "object_lon",
                   "object_date",
                   "object_time",
                   "object_depth_min",
                   "object_depth_max",
                   "object_lat_end",
                   "object_lon_end"
                   ]
    date = registers[0][0].find(' ')
    list_result = [stations[0][0],
                   stations[0][1],
                   registers[0][0][:10],
                   registers[0][0][11:],
                   stations[0][2],
                   stations[0][2],
                   stations[0][0],
                   stations[0][1],
                   ]
    return list_header, list_result


final_array = np.loadtxt(base_directory + 'IMPORT/modele_picture.tsv', max_rows=1, dtype=object)
final_array = final_array.reshape(1, (len(final_array)))
list_file, list_file_path = [], []
for path in result:
    if not path == 'Path not found':
        for root, directory, files in os.walk(path[0]):
            for dir in directory:
                for r, d, f in os.walk(root + '/' + dir + '/Uncropped'):
                    array_tsv = np.zeros((len(f)+1, len(final_array[0])), dtype=object)
                    array_tsv[0] = final_array[0]
                    for u in range(len(f)):
                        new_path = (r + '/' + f[u]).replace('Uncropped', 'Cropped')
                        if f[u] not in list_file:
                            list_file_path.append(new_path)
                            list_file.append(f[u])
                            stats_list = get_stats(new_path)
                            for i in range(len(stats_list[0])):
                                pos_key = int(np.where(final_array == stats_list[0][i])[0])
                                array_tsv[1+u, pos_key] = stats_list[1][i]
                            array_tsv[1 + u, 0] = f[u].replace('Uncropped', 'Cropped')  # img_file_name
                            array_tsv[1 + u, 1] = f[u][:-4].replace('Uncropped', 'Cropped')  # img_rank
                            db_list = get_metadata(f[u])
                            for i in range(len(db_list[0])):
                                pos_key = int(np.where(array_tsv[0] == db_list[0][i])[0])
                                array_tsv[1+u, pos_key] = db_list[1][i]

                            pos_id_cells = f[u][::-1].find('_')
                            array_tsv[1 + u, 11] = f[u][-pos_id_cells:-4].replace('Uncropped', 'Cropped')
                            array_tsv[1 + u, 12] = f[u].replace('Uncropped', 'Cropped')
                    array_tsv = array_tsv[~np.all(array_tsv == 0, axis=1)]
                    final_array = np.concatenate((final_array, array_tsv[1:, ]))
directory_name_pos = result[0][0][::-1].find('/')
directory_name = result[0][0][-directory_name_pos:]
os.mkdir(base_directory + "EXPORT SEADATANET/" + directory_name)
path_to_save = base_directory + "EXPORT SEADATANET/" + directory_name
np.savetxt(path_to_save + '/' + directory_name + ".tsv", final_array, delimiter='\t', fmt='%s')
for i in range(len(list_file_path)):
    list_file_path[i] = list_file_path[i].replace('Uncropped', 'Cropped')
    shutil.copyfile(list_file_path[i], path_to_save + '/' + list_file[i])
shutil.make_archive(path_to_save, "zip", path_to_save)

# Get model
print('End Test')