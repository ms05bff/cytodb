import os, sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from pathlib import Path

import numpy as np
import shutil

from cytodb.collectArray import CollectArray
from cytodb.function import function_files
from cytodb.inputs.pathDirectory import *
from cytodb.sqlite import exportTo_SQLite, export_picSQL


def replace_files(txt_files, csv_files, means_files):
    """
    Remove default file for analysis and SORT file by name
    :param txt_files: list of all txt files
    :param csv_files: list of all csv files
    :param means_files: list of all means files
    :return: list of csv and all default file
    """
    # Create new list of csv and default
    new_list, list_all = [], []
    for means_file in means_files:
        means_per_set = np.array(
            np.loadtxt(means_file, delimiter=";", dtype=str, usecols=0, skiprows=1)
        )
        column_ = np.array(
            np.loadtxt(means_file, delimiter=";", dtype=str, usecols=1, skiprows=1)
        )
        # Sort by name and group all files
        for txt in txt_files:
            same_folder = means_file[::-1].find("/")
            if txt.find(means_file[:-same_folder]) >= 0:
                pos_space = txt[::-1].find(" ")
                txt_key = txt[: -pos_space + 5]
                pos_last_slash = txt_key[::-1].find("/")
                key = txt_key[-pos_last_slash:]
                tmp_list = []
                # Get file with same pre_name
                for csv in csv_files:
                    if csv.find(txt_key) >= 0:
                        tmp_list.append(csv)
                i = 0
                default_true, start_row = 0, 0
                for j in range(len(means_per_set)):
                    if str(column_[j]) == "Default (all)":
                        default_true = 1
                    if means_per_set[j].find(key) >= 0:
                        start_row = j - i
                        i += 1
                list_set = column_[start_row + default_true: start_row + i]
                for item in tmp_list:
                    for set in list_set:
                        item_pos_name = item[::-1].find("/")
                        item_pos_space = item[-item_pos_name:].find(" ")
                        if set.find("Â") >= 0 and set.find("bil") >= 0:
                            set = set.replace("Â", "")
                        if item[-item_pos_name + item_pos_space:].find(set) >= 0:
                            new_list.append(item)
                        elif (
                            item.find(key + "_Listmode.csv") >= 0
                            and item not in list_all
                        ):
                            list_all.append(item)
    if not list_all:
        list_all = None
    return new_list, list_all


def run():
    """
    Browse CSV base to generate array and insert in database
    """
    # Before launching the script, test of CSV
    # launch_all_test()
    for main_root, main_directory, main_files in os.walk(home_directory):
        # Remove test directory
        # Create list of wrong folder because some files are corrupted
        for directory in main_directory:
            print("--------------------------------------------------------")
            # Initialize list for each kind of list of files
            cyz_files, csv_files, txt_files, means_files = [], [], [], []
            path = Path(main_root)
            path = str(path / directory)
            # Collect files in A TRAITER/
            (cyz_files, csv_files, txt_files, means_files) = function_files.get_files(
                path, cyz_files, csv_files, txt_files, means_files
            )
            # Get folder picture
            picture_path = function_files.get_picture_folder(path)
            new_path_picture = []
            if picture_path:
                new_path_picture.append(export_directory + "PICTURES/" + directory)
            else:
                if not picture_path:
                    new_path_picture = ["Path not found"]
            # Test open all files
            correct_files = function_files.test_files(csv_files, txt_files, means_files)
            # Fin cyz path
            if not cyz_files:
                cyz_files = function_files.find_cyz(data_to_add, directory)
            csv_files, all_files = replace_files(txt_files, csv_files, means_files)
            # Get the array
            if correct_files:
                array = CollectArray(
                    cyz_files,
                    csv_files,
                    txt_files,
                    means_files,
                    new_path_picture,
                    directory,
                    all_files,
                )

                final_array = array.run()
                if final_array:
                    exportTo_SQLite.export(export_directory, final_array)
                else:
                    print("Skipped this directory")
                # Copy folder picture in /base/pictures
                if new_path_picture[0] != "Path not found":
                    function_files.copy_picture(directory, picture_path, export_directory + 'PICTURES/')
                    export_picSQL.main(new_path_picture, final_array)
                # TODO : Add picture processing
                function_files.copy_csv(directory, path, export_directory + "CSV/")
                function_files.copy_cyz(directory, cyz_files, export_directory + "CYZ")
                print(directory)
            else:
                print("Skipped this directory")
                print(directory)
        # Move all files contained in "A TRAITER/IMPORT"
        for root, directory, files in os.walk(infos_directory):
            for file in files:
                for r, d, main_f in os.walk(base_directory + 'IMPORT/'):
                    if file in main_f:
                        os.remove(root + file)
                        shutil.move(infos_directory + file, base_directory + 'IMPORT/')
                    else:
                        shutil.move(infos_directory + file, base_directory + 'IMPORT/')
                    break
        # Move all files contained in "A TRAITER/CYZ"
        for root, directory, files in os.walk(data_to_add + 'CYZ/'):
            for dir in directory:
                try:
                    os.rmdir(data_to_add + "CYZ/" + dir)
                except:
                    print("Error while removing directory")
        break


def launch_all_test():
    pass


if __name__ == "__main__":
    # Launch after reading cytodb.doc
    run()
    print("End of the script")
