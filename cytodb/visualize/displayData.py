import matplotlib.pyplot as plt
import numpy as np
from cytodb.createConnexion import CreateConnexion
from cytodb.inputs.pathDirectory import *


def get_all_data(connexion, name):
    cur = connexion.conn.cursor()
    request = "SELECT " + name + " FROM values_echant"
    cur.execute(request)
    data = np.array(cur.fetchall())
    return data


def get_data_with_condition(connexion, name, condition):
    cur = connexion.conn.cursor()
    request = "SELECT " + name + " FROM values_echant " + " WHERE values_echant.mission = " + \
              "'" + condition + "'"
    cur.execute(request)
    data = np.array(cur.fetchall())
    return data


def main():
    database = export_directory + "pythonsqlite.db"
    connexion = CreateConnexion(database)
    with connexion.conn:
        FWS = get_all_data(connexion, "mean_total_fws")
        FLR = get_all_data(connexion, "mean_total_flr")
        FLO = get_all_data(connexion, "mean_total_flo")
        FWS_objectif_plancton = get_data_with_condition(connexion, "mean_total_fws", "objectifplancton")
        FLR_objectif_plancton = get_data_with_condition(connexion, "mean_total_flr", "objectifplancton")
        FLO_objectif_plancton = get_data_with_condition(connexion, "mean_total_flo", "objectifplancton")

    # Plot data
    fig = plt.figure(figsize=(19, 6))
    plt.subplot(131)
    # f(FWS) = FLR
    plt.scatter(FLR, FWS, c="red")
    plt.ylabel("FLR")
    plt.xlabel("FWS")
    plt.subplot(132)
    # f(FLR) = FLO
    plt.scatter(FLO, FLR, c="red")
    plt.ylabel("FLO")
    plt.xlabel("FLR")
    plt.subplot(133)
    # f(t) = FLR
    plt.plot(FLR)
    plt.xlabel("FLR")

    # Plot data
    fig = plt.figure(figsize=(19, 6))
    plt.subplot(131)
    # f(FWS) = FLR
    plt.scatter(FLR_objectif_plancton, FWS_objectif_plancton, c="red")
    plt.ylabel("FLR (objectif_plancton)")
    plt.xlabel("FWS (objectif_plancton)")
    plt.subplot(132)
    # f(FLR) = FLO
    plt.scatter(FLO_objectif_plancton, FLR_objectif_plancton, c="red")
    plt.ylabel("FLO (objectif_plancton)")
    plt.xlabel("FLR (objectif_plancton)")
    plt.subplot(133)
    # f(t) = FLR
    plt.plot(FLR_objectif_plancton)
    plt.xlabel("FLR (objectif_plancton)")
    plt.show()


if __name__ == "__main__":
    main()
    print("End")
