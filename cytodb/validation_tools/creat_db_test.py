# coding: utf-8

import os
import sqlite3 as lite

base_name='cyto_test.db'
os.remove(base_name)
con = lite.connect(base_name)
print("Opened test_database successfully")

with con:
    cur = con.cursor()
    cur.execute('''CREATE TABLE cyto_test([id] INTEGER PRIMARY KEY, [project] TEXT, [abund] INT, [vol] REAL,
                 [sampling_date] DATETIME, [QC] INT)''')
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex',81197537, 1220, DATETIME('2018-01-02 10:00:00'), 9)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 66415161, 1110, DATETIME('2018-01-03 12:00:00'), 1)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 46439864, 520, DATETIME('2018-01-04 10:00:00'), 9)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 46439864, 520, DATETIME('2018-01-05 10:00:00'), 2)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 46439864, 520, DATETIME('2018-01-06 10:00:00'), 3)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 46439864, 520, DATETIME('2018-01-07 10:00:00'), 4)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 46439864, 520, DATETIME('2018-01-08 10:00:00'), 5)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 46439864, 520, DATETIME('2018-01-09 10:00:00'), 6)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 46439864, 520, DATETIME('2018-01-10 10:00:00'), 7)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'daoulex', 46439864, 520, DATETIME('2018-01-11 10:00:00'), 8)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'mini', 60795612, 310, DATETIME('2019-01-02 12:00:00'), 5)")
    cur.execute("INSERT INTO cyto_test VALUES(NULL,'mini', 46439864, 312.3, DATETIME('2019-01-02 16:00:00'), 9)")

print("Table created successfully")
con.close()