# coding: utf-8

from validation_tools import time_ser
import pandas as pd
import sqlite3
from validation_tools import creat_db_test



con = sqlite3.connect('cyto_test.db')
query2 = "SELECT QC, project FROM cyto_test"
df2 = pd.read_sql_query(query2, con)
con.close()

counts = df2.groupby(['QC']).count()
counts = counts.reset_index('QC')
counts = counts.rename(columns={'project': 'count'})
counts['QC'] = counts['QC'].astype('str')

#s = df2.keywords
#counts = s.value_counts()
#percent = counts.value_counts(normalize=True)
#percent100 = counts.value_counts(normalize=True).mul(100).round(1).astype(str) + '%'
#pd.DataFrame({'counts': counts, 'per': percent, 'per100': percent100})

# Bokeh libraries
from bokeh.io import output_file, output_notebook
from bokeh.plotting import figure, show
from bokeh.transform import cumsum
import bokeh.palettes as p
from math import pi
from bokeh.models import ColumnDataSource
from bokeh.layouts import row, column, gridplot
from bokeh.models.widgets import Tabs, Panel


# Prepare the data

# Determine where the visualization will be rendered
output_file('test.html', title='plot cyto data')  # Render to static HTML, or
#output_notebook()  # Render inline in a Jupyter Notebook

# Set up the figure(s)

counts['angle'] = counts['count']/counts['count'].sum() * 2*pi
counts['color'] = p.RdYlGn[len(counts)]

p = figure(plot_height=300, title="Over all Validation", toolbar_location=None,
           tools="hover", tooltips="@QC: @count", x_range=(-0.5, 1.0))

p.wedge(x=0, y=1, radius=0.2,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend_field='QC', source=counts)   # , legend_field='QC'

p.axis.axis_label = None
p.axis.visible = False
p.grid.grid_line_color = None

show(p)

time_ser



