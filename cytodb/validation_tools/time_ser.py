import datetime
from os.path import dirname, join
import sqlite3
import pandas as pd
from scipy.signal import savgol_filter

from bokeh.io import curdoc, output_file
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, DataRange1d, Select
from bokeh.palettes import Blues4
from bokeh.plotting import figure, show

# bokeh serve --show time_ser.py

def get_dataset(src, name):
    mask = src['project'].values == name
    df = src[mask].copy()

    #df = src[src.airport == name].copy()
    #del df['airport']
    df['sampling_date'] = pd.to_datetime(df.sampling_date)
    # timedelta here instead of pd.DateOffset to avoid pandas bug < 0.18 (Pandas issue #11925)
    #df['left'] = df.date - datetime.timedelta(days=0.5)
    #df['right'] = df.date + datetime.timedelta(days=0.5)
    df = df.set_index(['sampling_date'])
    df.sort_index(inplace=True)

    return ColumnDataSource(data=df)

def make_plot(source, title):
    plot = figure(x_axis_type="datetime", plot_width=800, tools="", toolbar_location=None)
    plot.line('sampling_date', 'abund', source=source)

#    plot = figure(x_axis_type="datetime", plot_width=800, tools="", toolbar_location=None)
#    plot.title.text = title
#    plot.line('sampling_date', 'abund', source=source)

#    plot.quad(top='record_max_temp', bottom='record_min_temp', left='left', right='right',
#              color=Blues4[2], source=source, legend_label="Record")
#    plot.quad(top='average_max_temp', bottom='average_min_temp', left='left', right='right',
#              color=Blues4[1], source=source, legend_label="Average")
#    plot.quad(top='actual_max_temp', bottom='actual_min_temp', left='left', right='right',
#              color=Blues4[0], alpha=0.5, line_color="black", source=source, legend_label="Actual")

    # fixed attributes
    plot.xaxis.axis_label = "Time"
    plot.yaxis.axis_label = "Abundances"
    plot.axis.axis_label_text_font_style = "bold"
    plot.x_range = DataRange1d(range_padding=0.0)
    plot.grid.grid_line_alpha = 0.3

    return plot

def update_plot(attrname, old, new):
    project = project_select.value
    plot.title.text = "Cyto Samples for " + project

    src = get_dataset(df, project)
    source.data.update(src.data)

STATISTICS = ['nb_sample']

con = sqlite3.connect('cyto_test.db')
query = "SELECT * FROM cyto_test;"
df = pd.read_sql_query(query, con)
con.close()

counts = df.groupby(['project']).count()
list_proj = counts.index.tolist()

#query = "SELECT abund FROM cyto_test WHERE project == 'daoulex';"
#df = pd.read_sql_query(query, con)
#con.close()

project = list_proj[0]
projects = dict.fromkeys(list_proj, 1)

project_select = Select(value=project, title='Project', options=sorted(projects.keys()))

source = get_dataset(df, project)
plot = make_plot(source, "Cyto Samples for " + project)

project_select.on_change('value', update_plot)

controls = column(project_select)

#output_file('test2.html', title='plot cyto data')

curdoc().add_root(row(plot, controls))

#show(row(plot, controls))
curdoc().title = "Time Series"