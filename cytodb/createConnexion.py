import sqlite3
from sqlite3 import Error

from .inputs import table_list
from .inputs import table_picture


class CreateConnexion:
    def __init__(self, path_db):
        self.path = path_db
        self.conn = self.create_connection()

    def create_connection(self):
        """
        Create a database connection to the SQLite database specified by db_file
        :return: Connection object or None
        """
        try:
            conn = sqlite3.connect(self.path)
        except Error as e:
            print(e)
        return conn

    def create_all_tables(self):
        """
        Add tables to database
        """
        list_tables = [
            table_list.values_table,
            table_list.register_table,
            table_list.settings_table,
            table_list.missions_table,
            table_list.station_table,
            table_list.equipment_table,
            table_picture.picture_table,
            table_picture.pulse_table
        ]
        if self.conn is not None:
            for table in list_tables:
                self.create_table(table)
        else:
            print("Error! cannot create the database connection.")

    def create_table(self, create_table_sql):
        """
        Create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        """
        try:
            c = self.conn.cursor()
            c.execute(create_table_sql)
        except Error as e:
            print(e)

    def insertInto_table(self, sql, data):
        """
        Create a new task
        :param conn:
        :param task:
        :return:
        """
        cur = self.conn.cursor()
        cur.execute(sql, data)
        return cur.lastrowid
