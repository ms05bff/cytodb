import sqlite3
import sys

from PyQt5.QtWidgets import QApplication, QWidget, QMessageBox
from PyQt5.QtWidgets import QComboBox, QGridLayout, QLineEdit, QLabel, QPushButton

from cytodb.inputs.pathDirectory import *
from cytodb.sqlite.exportDB_toCSV import *


# Window developed in order to update value in the database
class App(QWidget):
    def __init__(self):
        super().__init__()
        # Path to the databse
        self.path_to_database = export_directory + "pythonsqlite.db"
        # Create parameters of the windows
        self.title = "Database - Get data from database"
        self.left = 200
        self.top = 200
        self.width = 720
        self.height = 480
        self.create_widget()
        self.initUI()

    def create_widget(self):
        self.lay = QGridLayout()
        # Second tabs
        self.label_table = QLabel("Souhaitez vous filtrer les données ?")
        self.comboChoice = QComboBox()
        self.message_box = QMessageBox()
        self.comboChoice.addItem(' ')
        self.comboChoice.addItem('Yes')
        self.comboChoice.addItem('No')
        self.comboChoice.currentIndexChanged.connect(self.changeItem)
        self.lay.addWidget(self.label_table, 0, 0)
        self.lay.addWidget(self.comboChoice, 0, 1)

    def initUI(self):
        # Initialisation of the windows
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        # self.add_widget()
        self.setLayout(self.lay)
        self.show()

    def confirm_pressed(self):
        if self.filter.text():
            filepath = main(self.comboItem.itemText(0),
                            self.comboItem.itemText(self.comboItem.currentIndex()),
                            self.comboTable.itemText(self.comboTable.currentIndex()),
                            self.filter.text())
        pop_up = self.message_box.setText("File saved : \n" + filepath)
        self.message_box.exec()

    def updateList(self):
        # Add table the first combo box
        self.connect = sqlite3.connect(self.path_to_database)
        cursor = self.connect.cursor()
        cursor.execute("SELECT * FROM sqlite_master")
        tables = cursor.fetchall()
        cursor.close()
        self.comboTable.addItem(' ')
        for i in range(len(tables)):
            self.comboTable.addItem(tables[i][1])

    def updateItem(self, tables):
        # Add item to the combo box
        self.connect = sqlite3.connect(self.path_to_database)
        self.comboItem.clear()
        cursor = self.connect.cursor()
        cursor.execute("SELECT * FROM " + tables)
        tables = cursor.description
        cursor.close()
        for i in range(len(tables)):
            self.comboItem.addItem(tables[i][0])

    def table_change(self):
        # Update item to the selected table
        if self.comboTable.itemText(self.comboTable.currentIndex()) != ' ':
            self.updateItem(self.comboTable.itemText(self.comboTable.currentIndex()))

    def get_max_id(self):
        # Return the max ID possible
        self.connect = sqlite3.connect(self.path_to_database)
        cursor = self.connect.cursor()
        request = "SELECT max(id_method) from METHOD"
        cursor.execute(request)
        max_id = cursor.fetchall()
        cursor.close()
        return max_id[0][0]

    def changeItem(self):
        for i in reversed(range(self.lay.count())):
            if not self.lay.itemAt(i).widget() == self.comboChoice:
                self.lay.itemAt(i).widget().setParent(None)

        # First item selected
        if self.comboChoice.currentIndex() == 1:
            self.label_table = QLabel("Appliquer un filtre sur quelle table ?")
            self.comboTable = QComboBox()
            self.filter = QLineEdit("")
            self.updateList()
            self.comboTable.currentIndexChanged.connect(self.table_change)
            self.label_item = QLabel("Et quel item ?")
            self.comboItem = QComboBox()
            self.confirmButton = QPushButton("Request")
            self.lay.addWidget(self.label_table, 1, 0)
            self.lay.addWidget(self.comboTable, 1, 1)
            self.lay.addWidget(self.label_item, 2, 0)
            self.lay.addWidget(self.comboItem, 2, 1)
            self.lay.addWidget(self.filter, 3, 1)
            self.lay.addWidget(self.confirmButton, 4, 1)

        # Second item selected
        elif self.comboChoice.currentIndex() == 2:
            self.confirmButton = QPushButton("Request")
            self.lay.addWidget(self.confirmButton, 1, 1)
        self.confirmButton.clicked.connect(self.confirm_pressed)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
