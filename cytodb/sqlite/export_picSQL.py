import shutil

import sys
import numpy as np
import pandas as pd
from scipy import stats
from cytodb.inputs.pathDirectory import *
from cytodb.pic_proc import picture_processing
from cytodb.createConnexion import CreateConnexion
from cytodb.inputs import table_picture


def main(path, data_array):
    print("Beginning of filtering")
    array_picture_info, array_pulse = [], None
    background_pic = None
    for pth in path:
        for main_root, main_directory, main_files in os.walk(pth):
            for sub_dir in main_directory:
                for root, directory, files in os.walk(main_root + "/" + sub_dir):
                    if len(files) == 1 and files[0].find("background") >= 0:
                        shutil.rmtree(root)
                        break
                    for file in files:
                        # First find if there is a background
                        if file.find("background") >= 0:
                            background_pic = file
                            break
                    for file in files:
                        # Then process
                        if file.find("background") <= 0:
                            # Informations about picture, file_name
                            (
                                picture_cropped,
                                information_picture,
                            ) = picture_processing.process(
                                file, root + "/", background_pic
                            )

                            if picture_cropped is not None:
                                values = collect_from_array(file, root, data_array)
                                pulse = picture_processing.find_pulse_file(root, main_root, file)
                                if values is not None and pulse is not None:
                                    if array_pulse is None:
                                        array_pulse = pulse
                                    else:
                                        array_pulse = np.dstack((array_pulse, pulse))
                                    array_picture_info.append(values)
                                picture_processing.save_picture(
                                    picture_cropped, file, root
                                )
                                picture_processing.keep_old_picture(root, file)
                            else:
                                picture_processing.remove_picture(root + "/" + file)
            break
    if array_picture_info is not None and array_pulse is not None:
        send_toSQL(array_picture_info, array_pulse)
    print("End of picture filtering")


def collect_from_array(file, root, data_array):
    pos_ = file.find('Uncropped')
    filename = file[:pos_-1]
    pos_id_cells = file[::-1].find('_')
    id_cells = file[-pos_id_cells:-4]
    root_pos = root[::-1].find('/')
    for i in range(len(data_array[1][1][0])):
        if data_array[1][1][0][i].find(filename) >= 0:
            for j in range(len(data_array[1][1][7][i:])):
                # Cas 'billes de 6um'
                if data_array[1][1][7][j].find('bil') >= 0 and data_array[1][1][7][j].find('6') >= 0 and \
                        root[len(root) - root_pos:].find('bil') >= 0 and root[len(root) - root_pos:].find('6') >= 0:
                    break
                # Cas 'billes de 1um'
                elif data_array[1][1][7][j].find('bil') >= 0 and data_array[1][1][7][j].find('1') >= 0 and \
                        root[len(root) - root_pos:].find('bil') >= 0 and root[len(root) - root_pos:].find('1') >= 0:
                    break
                elif root[len(root) - root_pos:].find(data_array[1][1][7][j]) >= 0:
                    break
            break
    path_file = data_array[1][1][1][i + j].replace('\\', '/')
    path_file = path_file.replace('A TRAITER/', '')
    list_values = [int(id_cells), path_file, file, data_array[1][1][8][i + j], file[:pos_-1]]
    return list_values


def get_mode(image):
    flatten_image = image.flatten()
    flatten_image = np.delete(flatten_image, np.where(flatten_image == 255))
    mode_object = stats.mode(flatten_image)[0]
    return mode_object


def send_toSQL(array_picture, array_pulse):
    list_tables = [
        table_picture.picture_header,
        table_picture.pulse_header,
    ]
    #
    table_to_insert = [
        table_picture.picture_insert,
        table_picture.pulse_insert,
    ]
    list_array = ["Particle ID", "Filename.csv", "Filename", "Groupe.Name",
                  "Filename.picture"]
    list_pulses = ['Particle ID', 'FWS', 'SWS', 'FL.Red', 'FL.Yellow', 'FL.Orange', 'FL.Green', 'Curvature']
    array = [[list_array, list_pulses], [np.array(array_picture, dtype=object), array_pulse]]

    path = export_directory + "pythonsqlite.db"
    connexion = CreateConnexion(path)
    array_sort = sort_arrayPIC(array, list_tables)
    test_and_insert(connexion, array_sort, table_to_insert)


def sort_arrayPIC(array, list_tables):
    """
    Sort array in 5 groups to fill the database
    :return: list of data
    """
    data_list = [[] for i in range(len(list_tables))]
    for key in list_tables[0]:
        if key in array[0][0]:
            pos_header = int(np.where(key == np.array(array[0][0]))[0])
            data_list[0].append(array[1][0][:, pos_header])
    for key in list_tables[1]:
        if key in array[0][1]:
            pos_header = int(np.where(key == np.array(array[0][1]))[0])
            data_list[1].append(array[1][1][:, pos_header, :])
    return data_list


def test_and_insert(connex, array, list_tables):
    """
    Check if data is already inserted
    :param connexion: connexion object to database
    :param array: array to test
    :return:
    """
    list_request = ["SELECT * FROM pictures",
                    "SELECT * FROM pulses"
                    ]
    data_to_insert = np.rot90(np.array(array[0])).tolist()
    test_in(connex, list_request[0], data_to_insert, list_tables[0])
    data_to_insert = split_pulse_array(np.array(array[1]))
    data_to_insert = np.rot90(data_to_insert).tolist()
    test_in(connex, list_request[1], data_to_insert, list_tables[1])


def test_in(connexion, request, data_to_insert, table):
    """
    Check if data is already inserted
    :param connexion: connexion object to database
    :param array: array to test
    :return:
    """

    with connexion.conn:
        cur = connexion.conn.cursor()
        # test stations
        cur.execute(request)
        result = cur.fetchall()

    for data in data_to_insert:
        data_in = False
        if not result == []:
            # data_in = False
            for rsl in result:
                rsl = list(rsl[1:])
                if data == rsl:
                    data_in = True
                    break
        if not data_in:
            with connexion.conn:
                row = tuple(data)
                connexion.insertInto_table(table, row)

    return True


def split_pulse_array(array):
    new_array = np.zeros((array.shape[0], array.shape[1]*array.shape[2]), dtype=object)
    for i in range(len(array)):
        new_array[i] = np.transpose(array[i]).flatten('C')
    return new_array
