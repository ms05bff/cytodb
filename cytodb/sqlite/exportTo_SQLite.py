import numpy as np
from datetime import timedelta, datetime
from ..createConnexion import CreateConnexion
from ..inputs import table_list


def export(path, array):
    """
    Different step to export data
    """
    list_tables = [
        table_list.values_csv,
        table_list.register_csv,
        table_list.settings_csv,
        table_list.missions_csv,
        table_list.station_csv,
        table_list.equipment_csv,
    ]
    #
    table_to_insert = [
        table_list.values_insert,
        table_list.register_insert,
        table_list.settings_insert,
        table_list.missions_insert,
        table_list.station_insert,
        table_list.equipment_insert,
    ]

    path = path + "pythonsqlite.db"
    connexion = CreateConnexion(path)
    connexion.create_all_tables()
    array_sort = sort_arrayCSV(array, list_tables)
    # For each array, insert data if not exist
    test_and_insert(connexion, array_sort, table_to_insert)
    print("Data inserted in pythonsqlite.db")


def sort_arrayCSV(array, list_tables):
    """
    Sort array in 5 groups to fill the database
    :return: list of data
    """
    data_list = [[] for i in range(len(list_tables))]
    # Browse all list
    max = [0] * len(array[0])
    for i in range(len(array[0])):
        for lst in array[1][i]:
            if max[i] < len(lst) and type(lst) is not str:
                max[i] = len(lst)
            elif type(lst):
                if not max[i]:
                    max[i] = 1
    list_count = 0
    for list in list_tables:
        for key in list:
            for i in range(len(array[0])):
                if key in array[0][i]:
                    pos_header = int(np.where(key == np.array(array[0][i]))[0])
                    if type(array[1][i][pos_header]) is str:
                        # Extend str to size of array
                        data_list[list_count].append([array[1][i][pos_header]] * max[i])
                    else:
                        data_list[list_count].append(array[1][i][pos_header])
        list_count += 1
    for i in range(9):
        if len(data_list[0][-1]) != len(data_list[0][i]):
            data_list[0][i] = [data_list[0][i][0]] * len(data_list[0][-1])
    return data_list


def test_and_insert(connex, array, list_tables):
    """
    Check if data is already inserted
    :param connexion: connexion object to database
    :param array: array to test
    :return:
    """
    list_request = ["SELECT * FROM values_echant",
                    "SELECT * FROM registers",
                    "SELECT * FROM settings",
                    "SELECT * FROM missions",
                    "SELECT * FROM stations",
                    "SELECT * FROM equipments"
                    ]
    for i in range(len(array)):
        # Remove all duplicate array
        unique_array = np.unique(np.array(array[i]), axis=1)
        # Rotate and convert to list
        if i == 1:
            for j in range(len(array[1][0])):
                if array[1][7][j] != 'No date found':
                    array[1][7][j] = datetime.strptime(array[1][7][j], "%Y-%m-%d %H:%M")
                    array[1][8][j] = datetime.strptime(array[1][8][j], "%Y-%m-%d %H:%M")
        data_to_insert = np.rot90(unique_array).tolist()
        test_in(connex, list_request[i], data_to_insert, list_tables[i])


def insertData(array_list, connexion, list_tables):
    """
    Insert data into table
    :param array_list: list of array
    :param connexion: connexion object to database
    """
    #
    table_count = 0
    with connexion.conn:
        for array in array_list:
            array = np.array(array)
            for i in range(array.shape[1]):
                row = tuple((array[:, i]))
                connexion.insertInto_table(list_tables[table_count], row)
            table_count += 1
        print("Data inserted in pythonsqlite.db")


def test_in(connexion, request, data_to_insert, table):
    """
    Check if data is already inserted
    :param connexion: connexion object to database
    :param array: array to test
    :return:
    """

    with connexion.conn:
        cur = connexion.conn.cursor()
        # test stations
        cur.execute(request)
        result = cur.fetchall()

    for data in data_to_insert:
        data_in = False
        if not result == []:
            data_in = False
            for rsl in result:
                rsl = list(map(str, rsl[1:]))
                if data == rsl:
                    data_in = True
                    break
        if not data_in:
            with connexion.conn:
                row = tuple(data)
                connexion.insertInto_table(table, row)

    return True


def remove_data(key_table, connexion):
    list_of_tables = [
        "values_echant",
        "grp_echant",
        "settings",
        "missions",
        "stations",
        "equipments",
    ]
    with connexion.conn:
        cur = connexion.conn.cursor()
        for lst in list_of_tables:
            request = "DELETE FROM " + lst + " WHERE directory_name = " + "'" + key_table + "'"
            cur.execute(request)
            connexion.conn.commit()
        cur.close()