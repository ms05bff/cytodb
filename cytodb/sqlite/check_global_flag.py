import numpy as np

from cytodb.createConnexion import CreateConnexion
from cytodb.inputs.pathDirectory import *

database = data_entry + "base/pythonsqlite.db"
connexion = CreateConnexion(database)

with connexion.conn:
    cur = connexion.conn.cursor()
    request = "SELECT cluster_flag, cytometer_flag, metadata_flag, samples_flag FROM values_echant"
    cur.execute(request)
    # Get all flag
    result_flag = np.array(cur.fetchall())
    cluster_flag = result_flag[:, 0]
    cytometer_flag = result_flag[:, 1]
    metadata_flag = result_flag[:, 2]
    samples_flag = result_flag[:, 3]
    # Create global flag array
    global_flag = np.zeros((len(cytometer_flag)),  dtype=str)
    for i in range(len(cytometer_flag)):
       global_flag[i] = str(int(cluster_flag[i]) + int(cytometer_flag[i]) + \
                     int(metadata_flag[i]) + int(samples_flag[i]))
    request = "UPDATE values_echant SET global_quality = ?"
    # Update every row of column global_quality
    cur.executemany(request, global_flag.tolist())
    connexion.conn.commit()
    cur.close()

print("Pythonsqlite have been updated")