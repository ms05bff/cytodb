import numpy as np

from cytodb.createConnexion import CreateConnexion
from cytodb.inputs.pathDirectory import *

# In order to convert data from database to CSV
# First we define value we want to get from each tables
# -------------------------------------------------------------------------- #
table_mesure_ench = (
    "filename, [trigger_level(mV)], [volume_echan(uL)], "
    "mean_total_fws, sd_total_fws, mean_total_sws, sd_total_sws, "
    "mean_total_flr, sd_total_flr, mean_total_flo, sd_total_flo, "
    "mean_total_flg, sd_total_flg, mean_total_fly, sd_total_fly,"
    "mean_length, sd_length"
)
# Second we need the corresponding header to csv
header_mesure_ench = [
    "File",
    "Trigger.level",
    "Volume",
    "Mean.Total.FWS_varx1",
    "SD.Total.FWS_varx2",
    "Mean.Total.SWS",
    "SD.Total.SWS",
    "Mean.Total.FLR",
    "SD.Total.FLR",
    "Mean.Total.FLO",
    "SD.Total.FLO",
    "Mean.Total.FLG",
    "SD.Total.FLG",
    "Mean.Total.FLY",
    "SD.Total.FLY",
    "Mean.Length",
    "SD.Length",
]
# We do this for each table
# -------------------------------------------------------------------------- #
table_register = "selection_set, groupe_name, " \
                 "[sampling_date(UTC)], [analysis_date(UTC)], abundance"
header_register = ["Selection.Set", "Standardized.name", "Sampling.Date",
                   "Analysis.Date", "Abundance"]
# -------------------------------------------------------------------------- #
table_method = "sws_ampli, flo_ampli, flr_ampli, trigger_channel"
header_method = [
    "SWS.amplification",
    "FLO.amplification",
    "FLR.amplification",
    "Trigger.Channel",
]
# -------------------------------------------------------------------------- #
table_mission = "project_name, project_start, project_end, standard_reference, " \
                "pi, samples_operator, platform_id, platform_type, platform_nat"
header_mission = ["Project", "Project.starting.Date", "Project.ending.Date",
                  "Standards.Reference", "PI", "Samples.Operator", "Platform.ID", "Platform.Type",
                  "Platform.Nationality"
                  ]
# -------------------------------------------------------------------------- #

# -------------------------------------------------------------------------- #
table_station = "area, [latitude(E)], [longitude(N)], nb_station, [depth(m)], " \
                "observ_type, clustering_method, ship_code, ship_name"
header_station = ["Study.area", "Latitude", "Longitude", "Station", "Depth",
                  "Observation.Type", "Clustering.Method", "Ship.Code", "Ship.Name"]
# -------------------------------------------------------------------------- #
table_equipment = "id_cytometer"
header_equipment = ["Cytometer.ID"]
# -------------------------------------------------------------------------- #


def fill_array(list_array):
    """
    Fill the array and save it as CSV file
    :param list_array: list of list of table header
    """
    # Define the function to fill the array and save in CSV
    path = input_directory + "cytodb/inputs/column_name.csv"
    header = np.loadtxt(path, delimiter=";", max_rows=1, dtype="U50")
    final_array = np.zeros((len(list_array[0]) + 1, len(header))).astype("U50")
    list_header_csv = [
        header_mesure_ench,
        header_register,
        header_mission,
        header_method,
        header_station,
        header_equipment,
    ]
    final_array[0] = header
    for i in range(len(list_array)):
        for j in range(list_array[i].shape[1]):
            pos_header = int(np.where(final_array[0] == list_header_csv[i][j])[0])
            final_array[1:, pos_header] = list_array[i][:, j]
    file = export_directory + "/EXPORT SEADATANET/DB_export.csv"
    filepath = saveCSV(final_array, file)
    return filepath


def saveCSV(array, filepath):
    """
    Save the file
    :param array: array to save
    :param directory_name: name of the directory where to save the file
    :param filename: name of the file
    """
    # Save CSV
    np.savetxt(filepath, array, delimiter=";", fmt="%s")
    print("File saved : " + filepath)
    return filepath


def main(id_item, item, table, filter ):
    """
    Export Database to CSV and Save it
    """
    # Create Connexion
    database = data_entry + "base/pythonsqlite.db"
    connexion = CreateConnexion(database)
    # Collect the data
    with connexion.conn:
        cur = connexion.conn.cursor()
        list_of_request = [
            table_mesure_ench,
            table_register,
            table_mission,
            table_method,
            table_station,
            table_equipment,
        ]
        list_of_tables = [
            "values_echant",
            "registers",
            "missions",
            "settings",
            "stations",
            "equipments",
        ]
        list_of_id = [
            "id_values_echant",
            "id_register",
            "id_mission",
            "id_information",
            "id_station",
            "id_equipment",
        ]
        # Exemple of how to filter the database : We want to filter with the project_name
        # So first, we select id_rows with correct project name and then we collect all rows with
        # same id

        request = (
            "SELECT " + id_item + " " + item + " FROM " + table + " WHERE " + item + "="
            + "'"
            + filter
            + "'"
        )
        # Execute request and get id
        cur.execute(request)
        id = np.array(cur.fetchall())[:, 0]
        list_id = id.flatten().tolist()
        id_str = ""
        for item in list_id:
            id_str = id_str + str(item) + ", "
        list_of_data = [[] for i in range(len(list_of_request))]
        nb_list = 0
        # Now we collect data
        # Request are separated because PC might not handle big request
        for i in range(len(list_of_request)):
            request = (
                "SELECT "
                + list_of_request[i]
                + " FROM "
                + list_of_tables[i]
                + " WHERE "
                + list_of_id[i]
                + " IN "
                + "("
                + id_str[:-2]
                + ")"
            )
            cur.execute(request)
            list_of_data[nb_list] = np.array(cur.fetchall())
            nb_list += 1
        filepath = fill_array(list_of_data)
        return filepath


if __name__ == "__main__":
    id_item, item = "id_cruise", "project_name"
    table = "cruise"
    filter = "objectifplancton"
    main(id_item, item, table, filter)
    print("End")
