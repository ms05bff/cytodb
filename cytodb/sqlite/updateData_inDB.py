import sys
import sqlite3
import numpy as np

from cytodb.inputs.pathDirectory import *
from PyQt5.QtWidgets import QApplication, QWidget, QMessageBox, QTabWidget
from PyQt5.QtWidgets import QComboBox, QGridLayout, QLineEdit, QLabel, QPushButton, QVBoxLayout


# Window developed in order to update value in the database
# noinspection PyAttributeOutsideInit
class App(QWidget):
    def __init__(self):
        super().__init__()
        # Path to the databse
        self.path_to_database = export_directory + "pythonsqlite.db"
        # Create parameters of the windows
        self.title = "Database - Change value"
        self.left = 200
        self.top = 200
        self.width = 720
        self.height = 480
        self.create_widget()
        self.initUI()

    def create_widget(self):
        self.lay = QVBoxLayout()
        # Initialize tab screen
        self.tabs = QTabWidget()
        self.tab1 = QWidget()

        # Add tabs
        self.tabs.addTab(self.tab1, "Modify")

        # Create first tab
        self.tab1.layout = QGridLayout(self)
        self.label_table = QLabel("Select a Table : ")
        self.label_item = QLabel("Select an Item : ")
        self.label_modified = QLabel("Change Item : From ")
        self.label_a = QLabel("to :")
        self.value_insert = QLabel("Value to insert : ")
        self.comboTable = QComboBox()
        self.comboItem = QComboBox()
        self.edit_First = QLineEdit("1")
        self.edit_Last = QLineEdit("1")
        self.message_box = QMessageBox()
        self.value = QLineEdit()
        self.validateButton = QPushButton("Submit Change")
        self.validateButton.clicked.connect(self.validate_pressed)
        self.tab1.layout.addWidget(self.label_table, 1, 0, 1, 2)
        self.tab1.layout.addWidget(self.comboTable, 1, 2, 1, 2)
        self.comboTable.currentIndexChanged.connect(self.table_change)
        self.tab1.layout.addWidget(self.label_item, 2, 0, 1, 2)
        self.tab1.layout.addWidget(self.comboItem, 2, 2, 1, 2)
        self.tab1.layout.addWidget(self.label_modified, 3, 0, 1, 1)
        self.tab1.layout.addWidget(self.edit_First, 3, 2, 1, 1)
        self.tab1.layout.addWidget(self.label_a, 3, 3, 1, 1)
        self.tab1.layout.addWidget(self.edit_Last, 3, 4, 1, 1)
        self.tab1.layout.addWidget(self.value_insert, 4, 0, 1, 2)
        self.tab1.layout.addWidget(self.value, 4, 2, 1, 1)
        self.tab1.layout.addWidget(self.validateButton, 5, 4, 1, 2)
        self.tab1.setLayout(self.tab1.layout)

    def initUI(self):
        # Initialisation of the windows
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        # self.add_widget()
        self.updateList()
        self.lay.addWidget(self.tabs)
        self.setLayout(self.lay)
        self.show()

    def updateList(self):
        # Add table the first combo box
        self.connect = sqlite3.connect(self.path_to_database)
        cursor = self.connect.cursor()
        cursor.execute("SELECT * FROM sqlite_master")
        tables = cursor.fetchall()
        cursor.close()
        for i in range(len(tables)):
            self.comboTable.addItem(tables[i][1])

    def updateItem(self, tables):
        # Add item to the combo box
        self.connect = sqlite3.connect(self.path_to_database)
        self.comboItem.clear()
        cursor = self.connect.cursor()
        cursor.execute("SELECT * FROM " + tables)
        tables = cursor.description
        cursor.close()
        for i in range(len(tables)):
            self.comboItem.addItem(tables[i][0])

    def table_change(self):
        # Update item to the selected table
        self.updateItem(self.comboTable.itemText(self.comboTable.currentIndex()))

    def validate_pressed(self):
        # When button is pressed, we check is data is correct and then
        # we update the values
        first = int(self.edit_First.text())
        last = int(self.edit_Last.text())
        if self.test_entry(first, last, self.value.text()):
            table = self.comboTable.itemText(self.comboTable.currentIndex())
            id = self.comboItem.itemText(0)
            item = self.comboItem.itemText(self.comboItem.currentIndex())
            first_id, last_id = self.edit_First.text(), self.edit_Last.text()
            value = self.value.text()
            request = (
                    "UPDATE "
                    + table
                    + " SET "
                    + item
                    + " = "
                    + value
                    + " WHERE "
                    + id
                    + " BETWEEN "
                    + first_id
                    + " AND "
                    + last_id
            )
            self.connect = sqlite3.connect(self.path_to_database)
            cursor = self.connect.cursor()
            cursor.execute(request)
            self.connect.commit()
            cursor.close()

    def get_table_id(self):
        list_id = []
        self.connect = sqlite3.connect(self.path_to_database)
        cursor = self.connect.cursor()
        cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
        table = cursor.fetchall()
        for tab in table:
            cursor.execute("SELECT * FROM " + tab[0])
            list_id.append(cursor.description[0][0])
        return table, list_id

    def test_entry(self, begin, end, value):
        value_max_id = self.get_max_id()
        # Test if ID filled is correct
        if begin < 1 or end > value_max_id:
            pop_up = self.message_box.setText("Wrong value of ID")
            self.message_box.exec()
            return False
        # Test if value is informed
        if not value:
            pop_up = self.message_box.setText("Value is missing")
            self.message_box.exec()
            return False
        else:
            # If everything is ok
            pop_up = self.message_box.setText("Value updated")
            self.message_box.exec()
            return True

    def get_max_id(self):
        # Return the max ID possible
        self.connect = sqlite3.connect(self.path_to_database)
        cursor = self.connect.cursor()
        table = self.comboTable.itemText(self.comboTable.currentIndex())
        column = self.comboItem.itemText(self.comboItem.currentIndex())
        request = "SELECT " + column + " FROM " + table
        cursor.execute(request)
        max_id = len(cursor.fetchall())
        cursor.close()
        return max_id


if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
