import os, sys
import shutil
import numpy as np


def get_files(path, cyz_files, csv_files, txt_files, means_files):
    """
    Browse folder to find and complet all list
    :param path: path to directory (/base/CSV/folder or files)
    :param cyz_files: empty list
    :param csv_files: empty list
    :param txt_files: empty list
    :param means_files: empty list
    :return: all list completed
    """
    for main_root, main_directory, main_files in os.walk(path):
        for file in main_files:
            file = main_root + "/" + file
            if file.find("means") >= 0 or file.find("set_statistic") >= 0:
                means_files.append(file)
            elif file[-3:] == "csv" and file.find("Listmode") > 0:
                csv_files.append(file)
            elif file[-3:] == "txt":
                txt_files.append(file)
            elif file[-3] == "cyz":
                cyz_files.append(file)
        for sub_directory in main_directory:
            #
            new_path = main_root + "/" + sub_directory
            get_files(new_path, cyz_files, csv_files, txt_files, means_files)
        break
    return cyz_files, csv_files, txt_files, means_files


def get_picture_folder(path):
    """
    Get path where are located all pictures
    :param path: path to directory (/base/CSV/folder or files)
    :return: path to picture folder
    """
    #
    path_dir_image = []
    sub_dir = False
    for main_root, main_directory, main_files in os.walk(path):
        for directory in main_directory:
            if directory.lower().find("image") >= 0:
                if not sub_dir:
                    path_dir_image.append(main_root)
                    break
                else:
                    path_dir_image.append(main_root + '/' + directory)
            else:
                sub_dir = True
                get_picture_folder(main_root + '/' + directory)
    return path_dir_image


def find_cyz(path, directory):
    """
    Get path where are located all pictures
    :param path: path to directory (/base/CSV/folder or files)
    :param directory: str of the current directory
    :return: path to picture folder
    """
    #
    path = path + 'CYZ/'
    name_mission, data_type = directory.split(' ')
    for main_root, main_directory, main_files in os.walk(path):
        for directory in main_directory:
            if directory.find(name_mission) >= 0:
                for sub_root, sub_directory, sub_files in os.walk(path + directory):
                    for sub_dir in sub_directory:
                        if sub_dir.find(data_type) >= 0:
                            for root, directory, files in os.walk(sub_root + '/' + sub_dir):
                                if files:
                                    return root
        else:
            return 'Path not found'


def test_files(csv_files, txt_files, means_files):
    """
    Test to open all previous gotten file
    :param csv_files: list of path of csv file
    :param txt_files: list of path of txt file
    :param means_files: list of path of means file
    :return: bool (True if correct)
    """
    all_files = csv_files + txt_files + means_files
    test = True
    if not (csv_files or means_files or txt_files):
        print("Files are missing (CSV or TXT or MEANS_PER_SET)")
        return False
    for file in all_files:
        try:
            array = np.array(np.loadtxt(file, delimiter=";", dtype=str))
        except:
            print("Error ! Cannont open file : " + file)
            test = False
    return test


def copy_picture(main_dir, path_of_picture, destination_path):
    """
    Copy picture folder from origin path to database
    :param main_dir: name of current folder used
    :param path_of_picture: root to folder
    :param destination_path: path where you want to copy
    """
    if path_of_picture:
        # Try if folder is already created
        try:
            os.mkdir(destination_path + main_dir)
        except:
            pass
        for pth in path_of_picture:
            for main_root, main_directory, main_file in os.walk(pth):
                if main_directory:
                    for directory in main_directory:
                        for rt, existing_directory, file in os.walk(
                            destination_path + main_dir
                        ):
                            if directory not in existing_directory:
                                src_path = main_root + "/" + directory
                                dest_path = destination_path + main_dir + "/" + directory
                                shutil.move(src_path, dest_path)
                                break
                            else:
                                print("Directory : " + directory + " already existing ")
                                break
                else:
                    for rt, existing_directory, file in os.walk(
                            destination_path + main_dir):
                        pos_directory = pth[::-1].find('/')
                        directory = pth[-pos_directory:]
                        if directory not in existing_directory:
                            dest_path = destination_path + main_dir + "/" + directory
                            shutil.move(pth, dest_path)
                            break
                        else:
                            print("Directory : " + directory + " already existing ")
                            break


def copy_csv(main_dir, data_path, destination_path):
    """
    Copy  folder from origin path to database
    :param main_dir: name of current folder used
    :param data_path: root to folder
    :param destination_path: path where you want to copy
    """
    for root, dir, file in os.walk(destination_path):
        if main_dir in dir:
            shutil.rmtree(destination_path + main_dir)
            shutil.move(data_path, destination_path + main_dir)
            break
        else:
            shutil.move(data_path, destination_path + main_dir)
            break
    print("Copy CSV folder : " + main_dir)
    print("From : " + data_path + " to : " + destination_path)


def copy_cyz(main_dir, data_path, destination_path):
    """
    Copy CYZ folder from origin path to database
    :param main_dir: name of current folder used
    :param data_path: root to folder
    :param destination_path: path where you want to copy
    """
    mission, type = main_dir.split(' ')
    try:
        os.mkdir(destination_path + '/' + mission)
    except:
        pass
    try:
        shutil.move(data_path, destination_path + '/' + mission + '/' + type)
        print("Copy CYZ folder : " + main_dir)
        print("From : " + data_path + " to : " + destination_path + '/')
    except:
        print("Didn't find CYZ files")