import numpy as np


def fill_(array_in, key_str, data):
    """
    Fill the array
    :param key_str: key of the column you want to fill
    :param data: data to fill
    """
    for i in range(len(key_str)):
        if not isinstance(data[i], list):
            try:
                data[i] = data[i].tolist() * (array_in.shape[0] - 1)
            except:
                data[i] = [data[i]] * (array_in.shape[0] - 1)
        key_date_seadatanet = int(np.where(array_in[0] == key_str[i].strip())[0])
        array_in[1:, key_date_seadatanet] = data[i]
