
thisdict = {
    "bil": "Standards beads",
    "bead": "Standards beads",
    "micro": "Microphyto",
    "pico": "Pico",
    "crypto": "Cryptophytes",
    "syn": "Synnechococcus-like",
    "nano": "Nano"
}


def match_set(grp_name):
    """
    Associate grp with Standardized Name
    :param grp_name: list of grp name
    :return: new list with right name
    """
    #
    standardized_name = []
    for item in grp_name:
        test_added = len(standardized_name)
        for key in thisdict:
            if item.lower().find(key) >= 0 and item.lower().find('org') >= 0:
                standardized_name.append('Org' + thisdict[key])
                break
            elif item.lower().find(key) >= 0 and item.lower().find('red') >= 0:
                standardized_name.append('Red' + thisdict[key])
                break
            elif item.lower().find(key) >= 0:
                if item.find('1') >= 0:
                    standardized_name.append(thisdict[key] + ' (1um)')
                elif item.find('6') >= 0:
                    standardized_name.append(thisdict[key] + ' (6um)')
                else:
                    standardized_name.append(thisdict[key])
                break
        if len(standardized_name) == test_added:
            standardized_name.append('No match found')

    return standardized_name
