import numpy as np


def get_calcul(mode, array):
    """
    Depend on the mode, get mean/std of column
    :param mode: 'means' or 'std'
    :param array: data array
    :return: stats of one column
    """
    # If we find ',' we replace by '.'
    if array.size == 1:
        return float(str(array).replace(',', '.'))
    else:
        for nb in range(len(array)):
            array[nb] = float(array[nb].replace(",", "."))
        if mode == "means":
            return round(np.mean(array.astype(float)), 4)
        elif mode == "std":
            return round(np.std(array.astype(float)), 4)


def get_stats(mode, key, files):
    """
    :param mode: 'means' or 'std'
    :param key: Header of the column to get the std
    :param files: path to the csv
    :return: return float value
    """
    data = [0] * len(files)
    for i in range(len(files)):
        # Identify the files
        array = np.array(np.loadtxt(files[i], delimiter=";", dtype=str, max_rows=1))
        # Get the right position of the column in the array
        pos_column = int(np.where(array == key)[0])
        column_key = np.array(
            np.loadtxt(files[i], delimiter=";", dtype=str, usecols=pos_column, skiprows=1))
        data[i] = get_calcul(mode, column_key)
    return data
