from datetime import timedelta, datetime

import numpy as np
import pandas as pd

from .function import function_stats
from .inputs.pathDirectory import *


def test_x_y_z(array):
    """
    Test if we have lat, long, depth in import file
    """
    if array[3].tolist() and array[4].tolist() and array[5].tolist():
        return True
    else:
        print("Coordinate is missing")
        return False


class ReadData:
    def __init__(self, csv_files, txt_files, means_files):
        self.csv_files = csv_files
        self.txt_files = txt_files
        self.means_per_set_files = means_files
        self.nb_set = self.get_same_nb_files()

    def get_same_nb_files(self):
        """
        Calculate the nb of file for one group
        :return: int
        """
        same_file_count, total_length = [], 0
        # Multiple means_files
        if len(self.means_per_set_files) != 1:
            for mean_file in self.means_per_set_files:
                total_length = 0
                data = np.array(np.loadtxt(mean_file, delimiter=";", dtype=str, usecols=0))
                second_column = np.array(np.loadtxt(mean_file, delimiter=";", dtype=str, usecols=1, skiprows=1))
                if data.shape[0] == 0:
                    print("means file : " + mean_file + " is empty")
                    return 1
                else:
                    for item in second_column:
                        if item.find('Default (all)') >= 0:
                            total_length += 1
                    same_file_count.append(len(data)-1-total_length)
            return same_file_count
        # Single means_files
        else:
            for mean_file in self.means_per_set_files:
                count_file = 0
                data = np.array(np.loadtxt(mean_file, delimiter=";", dtype=str, usecols=0))
                second_column = np.array(np.loadtxt(mean_file, delimiter=";", dtype=str, usecols=1, skiprows=1))
                if data.shape[0] == 0:
                    print("means file : " + mean_file + " is empty")
                    return 1
                else:
                    first_file = data[1]
                    start = 0
                    for name_file in data[1:]:
                        if name_file == first_file:
                            count_file += 1
                        else:
                            for i in range(start, start + count_file):
                                if second_column[count_file].find('Default (all)') >= 0:
                                    count_file -= 1
                            first_file = name_file
                            start = start + count_file
                            same_file_count.append(count_file)
                            count_file = 1
                    if second_column[len(data)-count_file-1] == 'Default (all)':
                        same_file_count.append(count_file-1)
                    else:
                        same_file_count.append(count_file)
                    return same_file_count

    def create_array(self):
        """
        Create empty array of the size of number of files
        :return: array
        """
        # Get all column named in CSV
        tableau_convert = input_directory + "cytodb/inputs/column_name.csv"
        array_seadatanet = np.array(
            np.loadtxt(tableau_convert, delimiter=";", dtype=str)
        )
        # Take off all spaces find in header
        for i in range(len(array_seadatanet)):
            array_seadatanet[i] = array_seadatanet[i].strip()
        array_seadatanet = np.append(array_seadatanet, ['Reference_cyz', 'Reference_csv',
                                                        'Picture_path', 'All_path'])
        length = self.length_array() + 1
        # Create array
        array_ = np.zeros((length, len(array_seadatanet))).astype(
            "U256"
        )  # U256 = Long string
        array_[0] = array_seadatanet
        return array_

    def length_array(self):
        """
        Verify if file means_per_set contains the right number of file
        """
        length = 0
        for file in self.means_per_set_files:
            array_means = np.array(
                np.loadtxt(file, delimiter=";", dtype=str, usecols=1, skiprows=1)
            )
            for row in array_means:
                if row.find('Default (all)') >= 0:
                    pass
                else:
                    length += 1
        return length

    def find_txt_infos(self, key_means):
        """
        Find informations in CSV or TXT file by key
        :param key_means: key associated to the data
        :return: list of informations
        """
        list_info = []
        list_key_means = ["SWS", "FL Red", "FL Yellow", "FL Orange", "FL Green"]
        u = 0
        for file in self.txt_files:
            file_info = [""] * len(key_means)
            # Open array
            data = np.array(np.loadtxt(file, delimiter=";", dtype=str, encoding='utf-8'))
            for i in range(len(data)):
                for j in range(len(key_means)):
                    # If find key in file
                    if data[i].find(key_means[j]) >= 0:
                        # And if this is a special key (from list_key_means)
                        if key_means[j] in list_key_means:
                            # We take the next rows as text and extract second bloc
                            file_info[j] = float(data[i + 1].split(":")[1].strip())
                        else:
                            if key_means[j] == "Volume":
                                vol = data[i].split(":")[1]
                                # Convert into float
                                file_info[j] = round(float(vol.replace(",", ".")), 2)
                            else:
                                # Take next word
                                file_info[j] = data[i].split(":")[1]
            list_info.append(file_info)
        return list_info

    def find_csv_infos(self, key_means):
        """
        Find data in the CSV
        key_means : list of str
        """
        file_info = [[] for i in range(3)]
        # Browse all means_per_set file
        for file in self.means_per_set_files:
            # Open the array
            list_to_delete = []
            data = np.array(np.loadtxt(file, delimiter=";", dtype=str))
            for i in range(data.shape[1]):
                # Browse keys
                for j in range(len(key_means)):
                    if key_means[j] == data[0, i]:
                        file_info[j].extend(data[1:, i].tolist())
            # If row "Default (all) finded, add to list_to_delete it
            for num_row in range(len(file_info[1])):
                if file_info[1][num_row].find("Default (all)") >= 0:
                    list_to_delete.append(num_row)
            for u in range(3):
                file_info[u] = np.delete(file_info[u], list_to_delete).tolist()
        return file_info

    def get_channel_triggered(self):
        """
        Browse info.txt to find which channel is TRIGGER
        want to find the data
        :return: string
        """
        list_trigger = []
        u = 0
        for file in self.txt_files:
            data = np.array(np.loadtxt(file, delimiter=";", dtype=str))
            for line in data:
                if "TRIGGER" in line:
                    list_trigger.append(
                        line[len("Channel 1:"): -len("- TRIGGER")].strip())
        return list_trigger

    def get_data_from_import(self, filepath, list_of_files):
        """
        Get data from csv "import_*****.csv"
        :param filepath: str
        :param list_of_files: str
        :return: list of list
        """
        # Load position of the three different part of the csv
        # Three parts are delimited by header
        key = ["Name", "Mission", "Name of mission"]
        df = pd.read_excel(filepath, header=None, usecols="A")
        array_start = [0, 0, 0]
        # We save where three differents array start
        for k in range(len(key)):
            array_start[k] = df.loc[df.values == key[k]].index[0]
        # First part of import
        first_key, first_result = self.get_project_import(filepath, array_start[0])
        # Second part of import
        second_key, second_result = self.get_mission_import(filepath, array_start[1])
        # If we have X,Y,Z we continue, if not we stop
        if test_x_y_z(second_result):
            # Third part of import
            third_key, third_result = self.get_date_import(filepath, list_of_files, array_start[2])
            if third_key:
                # key array of header
                key = first_key + second_key + third_key
                # Check if we find date in station
                result = first_result + second_result + third_result
                return key, result

    def get_project_import(self, filepath, array_start):
        key = [
            "PI",
            "Project",
            "Project.starting.Date",
            "Project.ending.Date",
            "Cytometer.ID",
            "Samples.Operator",
            "Standards.Reference",
            "Platform.Type",
            "Platform.ID",
            "Platform.Nationality",
        ]
        result = self.get_data_import(filepath, key, array_start)
        return key, result

    def get_mission_import(self, filepath, array_start):
        key = [
            "Station",
            "Ship.Name",
            "Ship.Code",
            "Depth",
            "Latitude",
            "Longitude",
            "Study.area",
            "Observation.Type",
            "Clustering.Method",
        ]

        result = self.get_data_import(filepath, key, array_start)
        for i in range(3, 6, 1):
            result[i] = result[i].astype(float)
        return key, result

    def get_date_import(self, filepath, list_of_files, array_start):
        key = [
            "File to process",
            "Station Associated",
            "Sampling Date",
            "Sampling Hour",
            "Sampling to UTC",
            "Processing to UTC",
            "Metadata.Flag",
            "Cluster.Flag",
            "Samples.Flag",
            "Cytometer.Flag"
        ]
        result = self.get_data_import(filepath, key, array_start)
        # Convert to datetime
        for i in range(len(result[2])):
            if result[2][i].find('?') >= 0:
                pass
            else:
                try:
                    datetime.strptime(result[2][i], "%Y-%m-%d %H:%M:%S")
                except:
                    d = datetime.strptime(result[2][i], "%d/%m/%Y")
                    result[2][i] = datetime.strftime(d, "%Y-%m-%d")
        processing_date, processing_hour = self.get_processing_date(list_of_files)
        # Delete from array all additional files
        third_result = self.test_extra_files(
            filepath, array_start, list_of_files, np.array(result)
        )
        # Get sampling and analysis date
        sampling_date, analysis_date = self.combine_date_hour(np.array(third_result[2:6]),
                                                     processing_date, processing_hour)

        for i in range(len(third_result[0])):
            if third_result[0][i].find(third_result[1][i]) >= 0:
                pass
            else:
                if len(np.unique(third_result[1])) == 1:
                    pass
                else:
                    if not third_result[0][i].find('remedios') >= 0:
                        print("Error in station associated : " + third_result[0][i])
                        return None, None
        key = ["Sampling.Date", "Analysis.Date"] + key[6:]
        result = [sampling_date] + [analysis_date] + third_result[6:]
        return key, result

    def combine_date_hour(self, list_date, processing_date, processing_hour):
        """
        Calculate the right time with UTC
        :param list_date: list of all date to update and jetlag to update
        :return: list of date
        """
        sampling, analysis = [], []
        for i in range(list_date.shape[1]):
            # If Sampling Date is missing
            if list_date[0][i].find('?') >= 0:
                sampling.append("No date found")
                analysis.append("No date found")
            else:
                # If only hour is missing
                if list_date[1][i].find('?') >= 0:
                    # We fill hour by "00h00"
                    list_date[1][i] = "00h00"
                # Combine Sampling Date, Sampling Hour, Sampling to UTC
                date_sampling = list_date[0][i].split(' ')[0] + " " + list_date[1][i].lower().replace("h", ":")[:5]
                delta = timedelta(hours=int(list_date[2][i]))
                date_sampling = datetime.strptime(date_sampling, "%Y-%m-%d %H:%M") + delta
                sampling.append(date_sampling.strftime("%Y-%m-%d %H:%M"))
                # Combine Analysis Date, Analysis Hour, Analysis to UTC
                date_analysis = processing_date[i].split(' ')[0] + " " + processing_hour[i].lower().replace("h", ":")[:5]
                delta = timedelta(hours=int(list_date[3][i]))
                date_analysis = datetime.strptime(date_analysis, "%Y-%m-%d %H:%M") + delta
                analysis.append(date_analysis.strftime("%Y-%m-%d %H:%M"))
        return sampling, analysis

    def get_data_import(self, path, list_key, pos_key):
        """
        Recover data from import_***.csv
        :param path: str to the csv
        :param list_key: list of str, header of column
        :param pos_key: int, row start of array
        :param one_line:
        """
        list_result = []
        data = pd.read_excel(path, skiprows=pos_key, header=None)
        filter = data.iloc[:, 0].tolist()
        pos_star = 0
        # Find row seperator
        for i in range(len(filter)):
            if str(filter[i]).find('***') >= 0 or str(filter[i]) == 'nan':
                pos_star = i
                break
        # open file in store in dataframe
        if pos_star != 0:
            data = pd.read_excel(path, skiprows=pos_key, nrows=i-1, dtype=str)
        else:
            data = pd.read_excel(path, skiprows=pos_key, nrows=i, dtype=str)
        # Find values by key
        for key in list_key:
            list_result.append(data[key].values.astype(str))
        return list_result

    def test_extra_files(self, path, array_start, list_of_files, data):
        """
        Compare file from import_***.csv to means_per_set 
        If files present in import and not means then we delete it
        """
        array = pd.read_excel(path, skiprows=array_start, dtype=str)
        list_index = []
        result = [[] for i in range(len(data))]
        for j in range(len(list_of_files)):
            list_of_files[j] = list_of_files[j][:-4]
        for k in range(len(array['File to process'].values)):
            if array['File to process'][k][-3:] == "cyz":
                array['File to process'][k] = array['File to process'][k][:-4]
        for file in list_of_files:
            if file in array['File to process'].values:
                list_index.append(array[array['File to process'] == file].index.values[0])
            else:
                print("File missing in : " + path + " is " + file)
        for i in list_index:
            for j in range(len(data)):
                result[j].append(data[j, i])

        return result

    def get_cells_stats(self):
        """
        Get stats for one column for each file
        :return: list of means/std for each column & file
        """
        list_key = [
            "FWS Total",
            "SWS Total",
            "FL Red Total",
            "FL Orange Total",
            "FL Green Total",
            "FL Yellow Total",
            "Sample Length",
        ]
        list_result = []
        for key in list_key:
            # For exemple get : Means FWS Total
            list_result.append(function_stats.get_stats("means", key, self.csv_files))
            # For exemple get : STD FWS Total
            list_result.append(function_stats.get_stats("std", key, self.csv_files))
        return list_result

    def get_processing_date(self, list_of_files):
        processing_date, processing_hour = [], []
        for file in list_of_files:
            if file.find('cyz') >= 0:
                processing_date.append(file[-20:-10])
                processing_hour.append(file[-9:-4])
            else:
                processing_date.append(file[-16:-6])
                processing_hour.append(file[-5:])
        return processing_date, processing_hour

