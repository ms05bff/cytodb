import numpy as np

from .inputs.pathDirectory import *
from .readData import ReadData
from .function import function_match_grp
from .function import function_fill


class CollectArray:
    def __init__(
            self,
            cyz_files,
            csv_files,
            txt_files,
            means_files,
            picture_path,
            mission_directory,
            all_path,
    ):
        self.cyz_files = cyz_files
        self.csv_files = csv_files
        self.readFiles = ReadData(csv_files, txt_files, means_files)
        self.picture_path = self.extend_picture_path(picture_path, self.readFiles.nb_set)
        self.all_path = self.extend_all_path(all_path, self.readFiles.nb_set)
        self.infoFile_path = self.findPath_info(mission_directory.split(" ")[0])
        self.final_array = None

    def extend_picture_path(self, paths, list_coef):
        """
        Extend to length of array
        :param paths: list of str
        :param list_coef: list of int
        :return: list extended
        """
        empty_list = []
        if len(paths) != 1:
            for i in range(len(paths)):
                empty_list.extend([paths[i]] * list_coef[i])
        else:
            empty_list = paths * np.sum(self.readFiles.nb_set)
        return empty_list

    def extend_all_path(self, paths, list_coef):
        """
        Extend to length of array
        :param paths: list of str
        :param list_coef: list of int
        :return: list extended
        """
        empty_list = []
        if paths is not None:
            for i in range(len(paths)):
                empty_list.extend([paths[i]] * list_coef[i])
        else:
            for i in range(len(list_coef)):
                for j in range(list_coef[i]):
                    empty_list.extend(['Path not found'])
        return empty_list

    def findPath_info(self, directory_name):
        """
        Find path to the right informations file /base/IMPORT
        :param directory_name: str
        """
        for root, directory, files in os.walk(infos_directory):
            for file in files:
                if file.lower().find(directory_name.lower()) >= 0:
                    return root + file

    def run(self):
        """
        Create and Collect data for Seadatanet array
        """
        self.final_array = self.readFiles.create_array()
        # get_stats = ClusterStats(readFiles.files)
        data = self.collect_data()
        if data:
            final_array = self.fill_data(data)
            return final_array

    def get_data(self):
        """
        Different step to get Seadatanet array
        """
        # First we collect all necessary data
        data = self.collect_data()
        # After we fill all the data
        final_array = self.fill_data(data)
        # return final_array

    def collect_data(self):
        """
        Collect all the data to fill Seadatanet array
        """
        key_infoTxt = ["Trigger level (mV)", "Flow rate", "Volume", "SWS",
                       "FL Red", "FL Yellow", "FL Orange", "FL Green"]
        # Get info of "list key" from "**Info.txt"
        info_from_infoTxt = self.readFiles.find_txt_infos(key_infoTxt)

        # Get info from "means_per_set_**.csv"
        key_meansCSV = ["Filename", "Set", "Concentration"]
        # Sometimes means_per_set is empty so we check if we have to repete data for each set
        means_info = self.readFiles.find_csv_infos(key_meansCSV)

        # Get Standardized Name for each set
        standardize_name = function_match_grp.match_set(means_info[1])

        # Get channel triggered
        channel_trigger = self.readFiles.get_channel_triggered()

        # Get all data from "import_*****.csv"
        data_from_match_csv = self.readFiles.get_data_from_import(
            self.infoFile_path, means_info[0]
        )
        if data_from_match_csv:
            # Get all data like "FL Red Total" ...
            means_cells = self.readFiles.get_cells_stats()

            # Get ship code and name of ship
            # ship_code = self.readFiles.get_nameOfShip(data_from_match_csv[1][11])

            # Create list of all data
            list_of_data = [
                info_from_infoTxt,
                means_info,
                standardize_name,
                channel_trigger,
                data_from_match_csv,
                means_cells,
            ]
        # return list_of_data
            return list_of_data

    def fill_data(self, data_list):
        """
        Fill array with all data (from data_list)
        datalist : list
        """
        # create two multiple list one for the header,
        # one for the data
        # Let's try something else
        # First of all create new array to fill new table
        # ------------------------------------------------------------ #
        volume = np.array(data_list[0])[:, 2].tolist()
        flow_rate = np.array(data_list[0])[:, 1].tolist()
        abundance = []
        for abund in data_list[1][2]:
            abd = abund.replace(",", ".")
            abundance.append(round(float(abd), 4))
        volum_echan, trigg_lvl = [], []
        for i in range(len(volume)):
            for j in range(self.readFiles.nb_set[i]):
                volum_echan.append(float(volume[i].replace(',', '.')))
                trigg_lvl.append(float(flow_rate[i].replace(',', '.')))
        count = np.array(np.array(abundance)*np.array(volum_echan), dtype=int)
        echan_header = [
            data_list[4][0][21],
            data_list[4][0][22],
            data_list[4][0][23],
            data_list[4][0][24],
            "Global.Quality",
            "Trigg_lvl",
            "Mean.Total.FWS_varx1",
            "SD.Total.FWS_varx2",
            "Mean.Total.SWS",
            "SD.Total.SWS",
            "Mean.Total.FLR",
            "SD.Total.FLR",
            "Mean.Total.FLO",
            "SD.Total.FLO",
            "Mean.Total.FLG",
            "SD.Total.FLG",
            "Mean.Total.FLY",
            "SD.Total.FLY",
            "Mean.Length",
            "SD.Length",
            "Total_Count",
            "Volume"
        ]
        echan_values = [
            data_list[4][1][21],
            data_list[4][1][22],
            data_list[4][1][23],
            data_list[4][1][24],
            [0],
            trigg_lvl,
            data_list[5][0],
            data_list[5][1],
            data_list[5][2],
            data_list[5][3],
            data_list[5][4],
            data_list[5][5],
            data_list[5][6],
            data_list[5][7],
            data_list[5][8],
            data_list[5][9],
            data_list[5][10],
            data_list[5][11],
            data_list[5][12],
            data_list[5][13],
            count.tolist(),
            volum_echan
        ]
        # ------------------------------------------------------------ #
        grp_echan_header = ["File",
                            "Reference_csv",
                            "Reference_cyz",
                            "Picture_path",
                            "All_path",
                            data_list[4][0][19],
                            data_list[4][0][20],
                            "Selection.Set",
                            "Standardized.name",
                            "Abundance",
        ]
        grp_echan_values = [
            data_list[1][0],
            self.csv_files,
            self.cyz_files,
            self.picture_path,
            self.all_path,
            data_list[4][1][19:][0],
            data_list[4][1][20:][0],
            data_list[1][1],
            data_list[2],
            abundance
        ]

        # ------------------------------------------------------------ #
        trigger_level = np.array(data_list[0])[:, 0].tolist()
        tmp_file_name = []
        j = 0
        for i in self.readFiles.nb_set:
            tmp_file_name.append(grp_echan_values[0][j])
            j = j + i

        for u in range(len(self.readFiles.nb_set)):
            trigger_level[u] = float(trigger_level[u].replace(',', '.'))
            flow_rate[u] = float(flow_rate[u].replace(',', '.'))
        amplification_array = np.array(data_list[0])[:, 3:].astype(float)
        infos_echantillon_header = [
            "Trigger.level",
            "Flow_rate",
            "Trigger.Channel",
            "SWS.amplification",
            "FLR.amplification",
            "FLY.amplification",
            "FLO.amplification",
            "FLG.amplification"
        ]
        infos_echantillon_values = [
                                trigger_level,
                                flow_rate,
                                data_list[3],
                                amplification_array[:, 0].tolist(),
                                amplification_array[:, 1].tolist(),
                                amplification_array[:, 2].tolist(),
                                amplification_array[:, 3].tolist(),
                                amplification_array[:, 4].tolist(),
        ]
        # ------------------------------------------------------------ #
        project_echan_header = data_list[4][0][0:10]
        project_echan_values = data_list[4][1][0:10]
        # ------------------------------------------------------------ #
        mission_echan_header = data_list[4][0][10:19]
        mission_echan_values = data_list[4][1][10:19]
        # ------------------------------------------------------------ #
        list_header = [echan_header] + [grp_echan_header] + [infos_echantillon_header] + \
                      [project_echan_header] + [mission_echan_header]
        list_values = [echan_values] + [grp_echan_values] + [infos_echantillon_values] + \
                      [project_echan_values] + [mission_echan_values]
        return list_header, list_values

    def resize_data(self, data_complet):
        """
        Get date from filename
        data_complet : full list
        """
        sampling_date = [0 for i in range(len(data_complet))]
        analysis_date = [0 for i in range(len(data_complet))]
        for i in range(len(data_complet)):
            space_pos = data_complet[i].find(" ")
            sampling_date[i] = data_complet[i][space_pos - 8: space_pos]
            analysis_date[i] = data_complet[i][space_pos:]
        return sampling_date, analysis_date
