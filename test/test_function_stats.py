import sys, os
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from cytodb.inputs.pathDirectory import *
from cytodb.function import function_stats

file = [
    test_directory + "micro_objplancton14092019_st1 2019-10-24 09h58_billes de 6µm_Listmode.csv"
]


class TestFunctionStats(unittest.TestCase):
    def test_StatsMean(self):
        # Manually calculate from EXCEL
        mean = 113.505546
        mean_from_function = function_stats.get_stats("means", "Arrival Time", file)
        self.assertEqual(round(mean, 2), round(mean_from_function[0], 2))

    def test_StatsSTD(self):
        # Manually calculate from EXCEL
        std = 8.779290
        std_from_function = function_stats.get_stats("std", "Sample Length", file)
        # We round at 1 because excel as less precision than numpy
        self.assertEqual(round(std, 1), round(std_from_function[0], 1))


if __name__ == "__main__":
    unittest.main()
