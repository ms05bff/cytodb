# Check if column in database are right
import sys, os
import datetime

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from cytodb.createConnexion import CreateConnexion
from cytodb.inputs.pathDirectory import *


# To get column, create request
def check_right_date(connexion):
    request = "SELECT  [analysis_date(UTC)], [sampling_date(UTC)] FROM registers"
    with connexion.conn:
        cur = connexion.conn.cursor()
        cur.execute(request)
    rows = cur.fetchall()
    for i in range(len(rows)):
        if rows[i][0] == 'No date found' or rows[i][1] == 'No date found':
            print("One sample has no date, n° : " + str(i))
            pass
        else:
            try:
                date1 = datetime.datetime.strptime(rows[i][0], "%d/%m/%Y %H:%M")
            except:
                date1 = datetime.datetime.strptime(rows[i][0], "%Y-%m-%d %H:%M")
            try:
                date2 = datetime.datetime.strptime(rows[i][1] , "%d/%m/%Y %H:%M")
            except:
                date2 = datetime.datetime.strptime(rows[i][1] , "%Y-%m-%d %H:%M")
            if date1 < date2:
                print("Error : Analysis date is before sampling")
                print(str(date1) + '/' + str(date2))
                break
    print('No date error')


# Create Connexion to database
path = export_directory + "pythonsqlite.db"
connexion = CreateConnexion(path)
check_right_date(connexion)
# Further we will add more verification
