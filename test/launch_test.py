import os, sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from cytodb.inputs.pathDirectory import *


for root, directory, files in os.walk(input_directory + "/test"):
	for file in files:
		if file != "__init__.py" and file != "launch_test.py":
			command = "python " + file
			os.system(command)
			print("-----------------------------------------------------")
	break

print("END : All test launched")