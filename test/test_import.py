import sys, os
import numpy as np
import pandas as pd
import datetime

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from cytodb.inputs.pathDirectory import *


def test_read(path):
    # test in there is no error in file
    try:
        array = pd.read_excel(path)
    except:
        print("Cannot open file")


def test_mission(path):
    data = pd.read_excel(path,  nrows=2, header=None)
    for data in data[1]:
        if data.find("?") >= 0:
            print("Wrong value in project")
            break


def test_project(path):

    data = pd.read_excel(path, header=None, usecols=[0])
    filter = data.iloc[:, 0].tolist()
    pos_star = 0
    # Find row seperator
    for i in range(len(filter)):
        if str(filter[i]) == "Mission":
            array_start = i
            break
    data = pd.read_excel(path, skiprows=array_start, header=None, nrows=2)
    correct_Value = True
    for value in data[1]:
        if value.find("?") >= 0:
            correct_Value = False
        if value.find("*") >= 0:
            correct_Value = False
    if not correct_Value:
        print("Wrong value in mission")


def test_wrong_date(path):
    # Test if there is any "?" inside date value
    key = "Name of mission"
    data = pd.read_excel(path, header=None, usecols=[0])
    # We save where the third array start
    filter = data.iloc[:, 0].tolist()
    # Find row seperator
    for i in range(len(filter)):
        if str(filter[i]) == key:
            array_start = i
            break

    csv_key = [
        "Sampling Date",
        "Sampling Hour",
        "Processing Date",
        "Processing Hour",
    ]
    data = pd.read_excel(path, skiprows=array_start, header=None)
    correct_date = True
    for key in csv_key:
        if correct_date:
            for header_nb in range(len(data.iloc[0])):
                if key == data.iloc[0][header_nb]:
                    for value in data[header_nb][1:]:
                        if str(value).find("?") >= 0:
                            correct_date = False
                        else:
                            pass
    if not correct_date:
        print("Wrong datetime value")


def main():
    for root, directory, files in os.walk(infos_directory):
        for file in files:
            if file != 'shipcode.xlsx' and file != 'modele_import.xlsx':
                print(file)
                test_read(root + file)
                test_mission(root + file)
                test_project(root + file)
                test_wrong_date(root + file)


main()
print("End of test_import.py")
