import os
import sys
import unittest
import numpy as np

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from cytodb.function import function_fill


class TestFunctionFill(unittest.TestCase):
    def test_fillArray(self):
        array_to_fill = np.array([["A", "B", "C"], [0, 0, 0], [0, 0, 0]], dtype="U50")
        array_to_get = np.array([["A", "B", "C"], [0, 42, 0], [0, 42, 0]], dtype="U50")
        function_fill.fill_(array_to_fill, ["B"], [42])
        np.testing.assert_array_equal(array_to_fill, array_to_get)


if __name__ == "__main__":
    unittest.main()
