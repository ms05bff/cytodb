import os
import sys
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

from cytodb.function import function_match_grp


class TestFunctionMatch(unittest.TestCase):
    def test_getMatch(self):
        grp = [
            "billes de 6µm",
            "billles de 1µm",
            "micro",
            "nano",
            "crypto",
            "pico",
            "syn",
            "picodezdzed",
            "micro 8de",
        ]
        grp_to_get = [
            "Standards beads (6um)",
            "Standards beads (1um)",
            "Microphyto",
            "Nano",
            "Cryptophytes",
            "Pico",
            "Synnechococcus-like",
            "Pico",
            "Microphyto",
        ]
        final_grp = function_match_grp.match_set(grp)
        self.assertEqual(final_grp, grp_to_get)


if __name__ == "__main__":
    unittest.main()
