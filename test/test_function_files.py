import sys, os
import shutil
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from cytodb.inputs.pathDirectory import *
from cytodb.function import function_files

result_list_cyz = []
result_list_csv = [
    test_directory
    + "micro_objplancton14092019_st1 2019-10-24 09h58_billes de 6µm_Listmode.csv",
    test_directory
    + "micro_objplancton14092019_st1 2019-10-24 09h58_OrgMicrophyto_Listmode.csv",
    test_directory
    + "micro_objplancton14092019_st1 2019-10-24 09h58_RedMicrophyto_Listmode.csv",
    test_directory
    + "micro_objplancton14092019_st2 2019-10-24 10h07_billes de 6µm_Listmode.csv",
    test_directory
    + "micro_objplancton14092019_st2 2019-10-24 10h07_OrgMicrophyto_Listmode.csv",
    test_directory
    + "micro_objplancton14092019_st2 2019-10-24 10h07_RedMicrophyto_Listmode.csv",
]
result_list_txt = [
    test_directory
    + "micro_objplancton14092019_st1 2019-10-24 09h58_Info.txt",
    test_directory
    + "micro_objplancton14092019_st2 2019-10-24 10h07_Info.txt",
]
result_list_means = [test_directory + "means_per_set_per_file.csv"]

folder = "microobj_plancton14042018_station1 2018-07-04 09h11_billes de 6µm_Images"
path_to_folder = test_directory


class TestFunctionFiles(unittest.TestCase):
    def test_getFiles(self):
        path = test_directory[:-1]
        list_cyz, list_csv, list_txt, list_means = [], [], [], []
        result_function = function_files.get_files(
            path, list_cyz, list_csv, list_txt, list_means
        )
        # Check list by list
        self.assertCountEqual(result_function[0], result_list_cyz)
        self.assertCountEqual(result_function[1], result_list_csv)
        self.assertCountEqual(result_function[2], result_list_txt)
        self.assertCountEqual(result_function[3], result_list_means)

    def test_TestFiles(self):
        self.assertTrue(
            function_files.test_files(
                result_list_csv, result_list_txt, result_list_means
            )
        )

    def test_picture(self):
        path = test_directory
        path_to_get = base_directory + "CSV/cytodb_test/"

        self.assertEqual(
            function_files.get_picture_folder(path)[0].replace("\\", "/"), path_to_get
        )

    def test_CopyFolder(self):
        directory = "objectifplancton14092018 micro"
        same_folder = True
        existing_dir = []
        # Get all path to directory to paste
        for root, dir, file in os.walk(test_directory):
            for d in dir:
                existing_dir.append(d)
            break
        # Call the function to move the folder
        function_files.copy_picture(
            directory, [path_to_folder], input_directory + "test/"
        )
        for root, d, files in os.walk(input_directory + "test/" + directory):
            for dir in d:
                shutil.copytree(root + '/' + dir, test_directory + '/' + dir)
        for root, dir, file in os.walk(input_directory + "test/" + directory):
            for d in existing_dir:
                if d in dir:
                    pass
                else:
                    same_folder = False
            break
        self.assertTrue(same_folder)
        # then we delete created folder
        path_to_delete = input_directory.replace('\\', '/') + "test/" + directory
        shutil.rmtree(path_to_delete)


if __name__ == "__main__":
    unittest.main()
