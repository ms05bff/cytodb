#coding utf-8

from database.ShowData import create_connection
import pandas as pd

def main():
    database = "../database/pythonsqlite.db"

    # create a database connection
    conn = create_connection(database)
    with conn:
        # Exemple of command to visualize data
        cur = conn.cursor()
        query1 = "SELECT date_start FROM Project"
        df1 =  pd.read_sql_query(query1, conn)

        # query2 = "SELECT QC, project FROM Mesure_Echantillon"
        # We wanna see all QC, and project for all Mesure_Echantillon
        # df2 = pd.read_sql_query(query2, conn)

        cur.execute("SELECT volume_ech FROM Mesure_Echantillon WHERE volume_ech<1000")
        rows = cur.fetchall()
        print('------------------------------------------------------')
        for row in rows:
            print(row)
        print('------------------------------------------------------')
        # We want to see all FLR and FWS where volume is higher than 1000
        cur.execute("SELECT Mean_Total_FWS, SD_Total_FWS, "
                    "Mean_Total_FLR, SD_Total_FLR FROM Mesure_Echantillon "
                    "WHERE volume_ech<1000")
        rows = cur.fetchall()
        for row in rows:
            print(row)
        print('------------------------------------------------------')


if __name__ == '__main__':
    main()
    print('End')