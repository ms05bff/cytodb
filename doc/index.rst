.. Cytobase documentation master file, created by
   sphinx-quickstart on Wed Nov 25 13:36:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cytobase documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./rst/project.rst
   ./rst/database.rst
   ./rst/modify_content_db.rst
   ./rst/requestDatabase.ipynb
   ./rst/export_CSV.rst
   ./rst/picture_process.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
