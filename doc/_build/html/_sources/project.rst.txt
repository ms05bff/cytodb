Cytobase
========
Introduction
-------------

CytoBS is a Python package developed to manage flow cytometry data. It allows user to store
a large amount of data in SQLite database. The language of the database has been chosen
in order to facilitate understanding of it and quickness access. But also because it is a
local project and for now is not subject to a further development.

To feed the database, it's necessary to give :

* Data File with CSV format (generally coming from CytoCluz)
* Complementary Data (Project Date, Place sample, see example) included in a file called
  "name_of_project.csv"at this path : "/base/CSV/IMPORT"

Database
--------

Below, the organizational diagram of the database. Tables and attributes might evolve in the
futur.

Diagram
~~~~~~~

.. image:: cytobase.png

Quality Flag
~~~~~~~~~~~~

* Metadata

+-------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------+
| Value | Label | Definition                                                                                                                                                                                                                           | Modified   |
+=======+=======+======================================================================================================================================================================================================================================+============+
| 0     | None  | No quality control procedures have been applied to the data value. This is the initial status for all data values entering the working archive.                                                                                      | 23/06/2020 |
+-------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------+
| 1     | Bad   | Data value adjusted during quality control. Best practice strongly recommends that the value before the change be preserved in the data or its accompanying metadata.                                                                | 23/06/2020 |
+-------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------+
| 2     | Mean  | Data value that is probably consistent with real phenomena but this is unconfirmed or data value forming part of a malfunction that is considered too small to affect the overall quality of the data object of which it is a part.  | 23/06/2020 |
+-------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------+
| 3     | Good  | Good quality data value that is not part of any identified malfunction and has been verified as consistent with real phenomena during the quality control process.                                                                   | 23/06/2020 |
+-------+-------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------------+

* Cluster

+-------+----------+--------------------------------------------+------------+
| Value | Label    | Definition                                 | Modified   |
+=======+==========+============================================+============+
| 0     |  Invalid | The cluster hasn't been check or is wrong. | 23/06/2020 |
+-------+----------+--------------------------------------------+------------+
| 1     |  Valid   | The cluster has been check and adjusted.   | 23/06/2020 |
+-------+----------+--------------------------------------------+------------+

* Cytometer

+-------+------------------+------------------------------------------------------------------------------------+------------+
| Value | Label            | Definition                                                                         | Modified   |
+=======+==================+====================================================================================+============+
| 0     |  Cytometer align | Cytometer has been align during analysis.                                          | 23/06/2020 |
+-------+------------------+------------------------------------------------------------------------------------+------------+
| 1     |  Beads added     | Beads has been added during analysis.                                              | 23/06/2020 |
+-------+------------------+------------------------------------------------------------------------------------+------------+
| 2     |  Align and Added | Cytometer has been align during analysis and Beads has been added during analysis. | 23/06/2020 |
+-------+------------------+------------------------------------------------------------------------------------+------------+


Project
-------------

The project has different features :

* Cytodb
* Tests
* Docs


First one, **cytodb** include all necessary script and file to complete database. Inside,
there is a lot of different folder. All this folder include different features of the
application.
* **function** define static function used in the project
* **input** define data enter from the developer to run the project
* **sqlite** define database function used to insert/modify/extract data
* **validations_tools** ...
* **visualize** contains function to display data from base. (Possibility to add feature as : Validate data with
different quality flag)

Third one, **docs** store documentation, picture of the database and example

Last feature, **test** includes unit test but also CSV test in order to check if it's
faithful to the smooth running of the project.
