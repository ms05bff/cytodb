
Insertion de données cytométriques dans la base de données SQLite : pythonsqlite.db
==================================================================================

Première étape : Préparez les données
-------------------------------------

Prévention : Faites attention au nom des dossiers, sous dossiers.

•	CSV

Lorsque les données ont été traitées par Cytocluz, il faut préparer les fichiers CSV dans
des dossiers différents. Pour cela, séparer les fichiers CSV en deux blocs «pico» et «micro».
Comme pour la mission daoulex2019, il y a un dossier «daoulex2019 micro» et
«daoulex2019 pico» comprenant les données en faisant attention à bien respecter l’espace
entre «daoulex2019» et «micro/pico». Une fois ces dossiers créés, déplacez-les à ce
chemin-ci : «/****/****/****/base/A TRAITER/CSV/».

•	CYZ

Les fichiers CYZ utilisaient par Cytocluz doivent également être placés dans un dossier
principal «daoulex2019» puis deux sous-dossiers «micro» et «pico». ATTENTION, ce n'est pas comme
pour les fichiers CSV. Une fois ces dossiers prêts, déplacez-les à ce chemin-ci :
«/****/****/****/base/A TRAITER/CYZ/».

•	IMPORT

Lorsque qu’un projet est traité, il est nécessaire de fournir un fichier
d’import comprenant les informations secondaires du projet (Longitude, Latitude,
Nom du projet, Date du projet, Heure de prélèvement, etc). Pour cela, il est important
de remplir ces informations dans le format fourni
(«/****/****/****/base/IMPORT/modele_import.csv »). Si vous ne savez
pas comment le remplir, vous pouvez regarder sur d’autres fichiers créés.
Une fois ce fichier correctement complété, déplacez-le à ce chemin-ci :
«/****/****/****/base/A TRAITER/IMPORT/».

.. image:: /pictures/modele_import.png
    :width: 700px
    :align: center

Voici quelques points IMPORTANTS à respecter concernant la réalisation d'un fichier import.

• Vérifier à ce qu'aucune case ne soit vide.
• En dessous de l'entête "Ship.Name", si vous ne connaissez pas le nom de votre bateau veuillez
indiquer "UNKNOWN" (correctement orthographié).  La liste des bateaux est consultable en page 2.
• Sous "File to process" veuillez indiquer le nom des fichiers CYZ entier.
• Vérifier une deuxième fois que les fichiers ont été correctement complétés.



Deuxième étape : Déplacez les données
-------------------------------------

Lorsque toutes les données sont prêtes à être exportées, pour démarrer l’exportation de
celle-ci : situez-vous dans le dossier «cytodb/» et lancez un terminal Linux. Tapez
la commande qui suit: «python3 -m cytodb». Attention, certains packages sont nécessaires au
fonctionnement du script (python3-sqlite3, python3-numpy, python3-path). Il est également
possible de lancer le script depuis PyCharm (terminal ou mode run/debug, RECOMMANDÉ).

Troisième étape: Consultez les données
--------------------------------------

Pour consulter les données, il existe plusieurs méthodes. En voici deux :

1.	A partir d’un logiciel externe «DB Browser for SQLite» pour Linux ou Windows, il
suffit de sélectionner la base de données puis de sélectionner «Ouvrir une base de données
existante» depuis le logiciel. (Solution recommandée).

2.	Il est possible d’ouvrir cette base depuis un terminal. Il faudra installer le package
sqlite3 («sudo apt-get install sqlite3») et connaître les requêtes SQL nécessaires pour
afficher ce dont vous souhaitez.



