

Intéraction avec les données cytométrique dans la base de données SQLite : pythonsqlite.db
==================================================================================================

Modification des données
~~~~~~~~~~~~~~~~~~~~~~~~

Les données de la base peuvent être modifiées depuis l’interface ci-dessous.
Il existe deux manières d’ouvrir cette fenêtre :

1.	Depuis PyCharm, en lançant le mode run/debug du fichier «updateData_inDB.py»
ou alors depuis le terminal PyCharm. Tapez la commande suivante (une fois situé
dans le dossier du fichier) : «python updateData_inDB.py»

2.	Depuis un terminal Ubuntu, installer les packages pré-requis (python3, python3-pyqt5)
et lancez la commande suivant: «python3 updateData_inDB.py»

.. figure:: ../pictures/modify.png
    :width: 700px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Figure - Database Modify Window

    A partir de cette fenêtre, il est possible de sélectionner, la table et les colonnes
    associées afin d'y modifier la valeur. Lorsque la table est selectionée, vous pouvez
    choisir l'item (la colonne). Ensuite, vous choisissez l'ID de début et de fin de
    modification puis vous validez.

Suppression des données
~~~~~~~~~~~~~~~~~~~~~~~~

Etape 1 :

Afin de supprimer des données incorrectes ou en surplus, il existe trois possibilités depuis cette fenêtre. Lorsque
vous lancerez le programme (comme pour modifier, il suffit juste de changer d'onglet) vous apercevrez une barre
cliquable qui vous propose : Suppression unique, multiple ou par rangée.


.. |pic1| image:: /pictures/delete_1.png
    :width: 700

.. |pic2| image:: /pictures/delete_2.png
    :width: 700

.. table:: Exemple
   :align: center

   +--------+--------+
   | |pic1| | |pic2| |
   +--------+--------+

Etape 3 :

.. figure:: /pictures/delete_3.png
    :width: 700px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Figure - Suppression d'une ligne

    Cette option là, vous permez de supprimer une ligne de la base de données
    en indiquant son identifiant. (Consulter la base de données afin de savoir
    ce que vous souhaitez supprimer)

Etape 4 :

.. figure:: /pictures/delete_4.png
    :width: 700px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Figure - Suppression de plusieurs lignes

    Même option que précédemment, cependant celle-ci permet d'indiquer plusieurs lignes.
    Pour cela, il suffit d'espacer les identifiants de chaque ligne par un espace. (2 3 4 5 ...)

Etape 5 :

.. figure:: /pictures/delete_5.png
    :width: 700px
    :align: center
    :alt: alternate text
    :figclass: align-center

    Figure - Suppression de plusieurs lignes

    Cette option permet de supprimer une plage de données. Il suffit d'y indiquer le début et la fin
    de la plage souhaité.