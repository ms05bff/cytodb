

Processus d'analyse et filtrage des images
==========================================

Le projet Cytodb inclut un script permettant le filtrage des images comprises
dans le dossier PICTURES de la base de données.

En exécutant le script, celui-ci va filtrer les images suivant deux critères :

- Si l'image contient un élément à extraire
- Si l'image est assez nette

De ce fait, les images ne répondant à aucun de ces deux critères seront supprimés. Pour ceux restantes,
elles sont extraites depuis l'image de base dans deux tailles différentes originales ('Uncropped et 'Cropped').

Image avant traitement / Après traitement :

.. |pic1| image:: /pictures/uncropped.jpg
    :width: 700

.. |pic2| image:: /pictures/cropped.jpg
    :width: 100

.. table:: Uncropped / Cropped
   :align: center

   +--------+--------+
   | |pic1| | |pic2| |
   +--------+--------+

Celles-ci sont disposées dans des dossiers différents. Vous trouverez ci-dessous un exemple.

Avant lancement :

.. image:: /pictures/before_process.png
    :width: 700


Après lancement :

.. image:: /pictures/after_process.png
    :width: 700

L'ensemble a été distribué dans les deux dossiers.
