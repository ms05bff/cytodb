
Extraire les données au format CSV
===================================

Comment faire ?
---------------

Afin de récupérer les données au format CSV depuis la base de données, il est impératif
d'utiliser une fenêtre spécifique. Pour cela rendez-vous à l'emplacement
cytodb/sqlite/window_CSV_export.py depuis un terminal linux et écrivez la commande :
"python3 window_CSV_export.py". Il existe également une deuxième possibilité pour ouvrir
la fenêtre, depuis Pycharm lorsque le projet est ouvert rendez vous sur le fichier
"window_CSV_export.py" et lancez le fichier en mode Run (Maj + F10), cette dernière façon
est la plus recommandée.

Suite à cela, une fenêtre devrait s'ouvrir. Vous apercevrez les photos ci-dessous :
Vous avez donc la possiblité de décider de filter les données (recommandé) ou non.
Si vous choissisiez de ne pas filtrer les données *(à ajouter)*, le CSV contiendra toutes
les données de la BDD.

.. |pic1| image:: /pictures/export_1.png
    :width: 700

.. |pic2| image:: /pictures/export_5.png
    :width: 700

.. table:: Exemple
   :align: center

   +--------+--------+
   | |pic1| | |pic2| |
   +--------+--------+

Filtrer les données récupérées :
Cette option-ci vous permez d'appliquer un filtre sur les données récupérées.

.. |pic3| image:: /pictures/export_2.png
    :width: 700

.. |pic4| image:: /pictures/export_3.png
    :width: 700

.. table:: Exemple
   :align: center

   +--------+--------+
   | |pic3| | |pic4| |
   +--------+--------+

Enfin, l'option sans filtrage :

.. image:: /pictures/export_4.png
    :width: 700px
    :align: center

Lorsque vous avec décidé d'extraire les données, appuyez sur le bouton "Request".
Un message devrait alors apparaître :

.. image:: /pictures/export_6.png
    :width: 700px
    :align: center

Celui-ci confirme le bon import des données et vous indique le chemin afin d'y accéder.