Cytobase
========
Introduction
-------------

Cytobase est un logiciel de bancarisation de données biologiques. Actuellement développé en Python 3.7, il permet de
stocker dans une base de données (BDD) SQLite un nombre important de valeurs. La BDD choisit est SQLite pour sa
faciliter de compréhension et sa rapidité d'accès. De plus, le projet évolue localement et n'est pas sujet
(pour l'instant) à évoluer publiquement.

Pour insérer des données, il est nécessaire de fournir :
* des fichiers de données au format CSV (généralement généré depuis Cytocluz)
* des données complémentaires (Date de projet, endroit de prélèvement, voir exemple) contenus dans un fichier intitulé
"import_name_of_project.csv" à ce chemin-ci : "/base/CSV/IMPORT"


CytoBS is a Python package developed to manage flow cytometry data. It allows user to store
a large amount of data in SQLite database. The language of the database has been chosen
in order to facilitate understanding of it and quickness access. But also because it is a
local project and for now is not subject to a further development.

To feed the database, it's necessary to give :

* Data File with CSV format (generally coming from CytoCluz)
* Complementary Data (Project Date, Place sample, see example) included in a file called
  "name_of_project.csv"at this path : "/base/A TRAITER/IMPORT"

Database
--------

Ci-dessous, l'organisation de la base de données. Les tables et les attributs
peuvent évoluer dans le futur.

Schéma
~~~~~~~

.. image:: /pictures/cytobase.png
    :width: 55%
    :align: center

Drapeau de qualité
~~~~~~~~~~~~~~~~~~

* Metadata

+-------+---------+-----------------------------------------------------------------------------------------+------------+
| Value | Label   | Definition                                                                              | Modified   |
+=======+=========+=========================================================================================+============+
| 0     | Unknown | Metadata unknown.                                                                       | 13/01/2021 |
+-------+---------+-----------------------------------------------------------------------------------------+------------+
| 1     | Partial | Only partial                                                                            | 13/01/2021 |
+-------+---------+-----------------------------------------------------------------------------------------+------------+
| 2     | Main    | Main  metadata informations are included                                                | 13/01/2021 |
+-------+---------+-----------------------------------------------------------------------------------------+------------+
| 3     | Full    | All metadata informations are included                                                  | 13/01/2021 |
+-------+---------+-----------------------------------------------------------------------------------------+------------+

* Clustering

+-------+----------+--------------------------------------------+------------+
| Value | Label    | Definition                                 | Modified   |
+=======+==========+============================================+============+
| 0     |  Invalid | The cluster is wrong.                      | 13/01/2021 |
+-------+----------+--------------------------------------------+------------+
| 1     |  Unknown | The cluster hasn't been check.             | 13/01/2021 |
+-------+----------+--------------------------------------------+------------+
| 2     |  Valid   | The cluster has been check.                | 13/01/2021 |
+-------+----------+--------------------------------------------+------------+

* Sample

+-------+-------------+-------------------------------------------------------------------------------+------------+
| Value | Label       | Definition                                                                    | Modified   |
+=======+=============+===============================================================================+============+
| 0     |  Delay > 2h | Delay between samples and fixation is higher than 2hours.                     | 02/02/2021 |
+-------+-------------+-------------------------------------------------------------------------------+------------+
| 1     |  Delay > 1h | Delay between samples and fixation is higher than 1hour.                      | 02/02/2021 |
+-------+-------------+-------------------------------------------------------------------------------+------------+
| 2     |  Delay < 1h | Delay between samples and fixation is less than 1hour.                        | 02/02/2021 |
+-------+-------------+-------------------------------------------------------------------------------+------------+
| 3     |  No delay   | No delay between analyse and sample.                                          | 02/02/2021 |
+-------+-------------+-------------------------------------------------------------------------------+------------+

* Cytometer

+-------+------------------------+--------------------------------------------------------------------------------+------------+
| Value | Label                  | Definition                                                                     | Modified   |
+=======+========================+================================================================================+============+
| 0     |  Bad Calibration       | Bad laser alignment and/or pump not calibrated before analysis.                | 13/01/2021 |
+-------+------------------------+--------------------------------------------------------------------------------+------------+
| 1     |  Calibration Unchecked | Calibration has not been checked.                                              | 13/01/2021 |
+-------+------------------------+--------------------------------------------------------------------------------+------------+
| 2     |  Doubtful Alignment    | Alignment before analysis but moved during deployment without beads addition.  | 13/01/2021 |
+-------+------------------------+--------------------------------------------------------------------------------+------------+
| 3     |  Good Alignment        | Good alignment before and after deployment but without beads addition.         | 13/01/2021 |
+-------+------------------------+--------------------------------------------------------------------------------+------------+
| 4     |  Align and Beads       | Good alignment and beads has been added during analysis.                       | 13/01/2021 |
+-------+------------------------+--------------------------------------------------------------------------------+------------+

* Global Flag

+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| Value     | Label     | Definition                                                                                | Modified   |
+===========+===========+===========================================================================================+============+
| 0         | Bad       | if  Cytometer flag < 1  (Bad calibration)                                                 | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| 1         | Unkown    | if  Cytometer flag = 1  (Unknown calibration)                                             | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| 2         | Doubtful  | if  Cytometer flag = 2   (Alignment changed during the sampling)                          | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| 3         | Very Low  | if  Cytometer flag > 2 & clustering = 0  (Cluster pb)                                     | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| 4-5       | Low       | if  Cytometer flag > 2 & clustering = 1 & samples = 0 (Delay >1h)                         | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| 6         | Low       | if  Cytometer flag > 2 & clustering = 1 & samples = 1 (Delay <1h)                         | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| 7         | Good      | if  Cytometer flag > 2 & clustering = 1 & samples > 2 (No delay)                          | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| 8-9       | Very Good | if  Cytometer flag > 2 & clustering = 2 & samples = 3 & metadata <= 1  (Missing Metadata) | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+
| 10-11-12  | Very Good | if  Cytometer flag > 2 & clustering = 2 & samples = 3  & metadata  > 1                    | 02/02/2021 |
+-----------+-----------+-------------------------------------------------------------------------------------------+------------+



Projet
------

Le projet comporte différents blocs :

* Cytodb
* Tests
* Doc


Premièrement, **cytodb** intègre tous les scripts et fichiers nécssaires pour compléter la base de données.
Chaque sous-dossier regroupe différentes fonctionnalités développer au cours du projet.

* **function** définit les fonctions statiques utilisées dans le projet
* **input** définit pour les fichiers de données internes pour le projet
* **sqlite** définit les fonctions pour insérer ou extraire dans la base de données
* **pic_proc** définit les méthodes de calculs de traitement d'images et leur intégration à la base de données
* **validations_tools** ...
* **visualize** contient les fonctions pour afficher les données depuis la base de données.

(Possibilité d'ajouter des fonctionnalités comme la validation des données et la
modification des drapeaux de qualités)

Deuxièmement, **doc** comprend la documentation du projet et le modèle de la base de données.

Dernière fonctionnalité, **test** inclut les tests unitaires mais également des tests pour les CSV de
de façon à controller si le programme peut se lancer ou non.