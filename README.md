# CytoDB

Python package developped to manage locally flowcytometry data after export from Cytoclus4

- SQLite database

![Image](./doc/pictures/cytobase.png?raw=true)

- Scripts for export to CSV data (SeadataNet format) with associated metadata


